Graphes des nombres de signataires ce dimanche 13 juin 2010 à 8h52
##################################################################
:date: 2010-06-13 06:59
:category: Legislatives 2010
:slug: graphes-des-nombres-de-signataires-ce-dimanche-13-juin-2010-a-8h52
:status: published

| |\\"\\"|\\r\ |\\"\\"|\\r

(voir aussi la `page principale de l'outil
de\\rsuivi <\"../legis2010/\">`__ pour les détails notamment nominatifs,
en particulier sur la `liste des
signataires <\"http://lepacte.be/legis2010/index.php?action=liste_signataires&o_additional_position_in_list=1&o_sens=ASC#table_candidats\">`__)

.. raw:: html

   </p>

.. |\\"\\"| image:: \"/legis2010/graph/chart201006130800a.png\"
.. |\\"\\"| image:: \"/legis2010/graph/chart201006130800b.png\"
