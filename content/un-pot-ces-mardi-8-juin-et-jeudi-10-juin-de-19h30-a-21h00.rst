Un pot ces mardi 8 juin et jeudi 10 juin de 19h30 à 21h00
#########################################################
:date: 2010-06-06 16:40
:category: Legislatives 2010
:slug: un-pot-ces-mardi-8-juin-et-jeudi-10-juin-de-19h30-a-21h00
:status: published

J-7 : en route les amis, il faut se voir et fêter nos premiers succès,
mais aussi convenir des dernières actions. Nous vous proposons de nous
retrouver pour ces second et troisième pot de la campagne 2010, histoire
de toujours mettre des visages et des voix\\rsur des noms et adresses
email.

Date: **mardi 8 juin et jeudi 10 juin 2010 de 19h30 à 21h00 **

\\r

Lieu : « **Etc...** », petit bistrot de quartier\\rmodernisé avec
terasse situé à Etterbeek – La Chasse, avenue Jules\\rMalou, fréquenté
par quelques libristes avec wifi pour les amis.

\\r

Merci de compléter le
`papillon <\"http://papillon.peacefrogs.net/poll/5ZDqO91275842695\">`__
pour indiquer votre présence possible ou vraisemblable, ainsi que celle
de vos amis.

\\r

La localisation sur la carte libre
`OpenStreetMap <\"http://www.osm.org/\">`__ est fournie
`http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17 <\"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\">`__
(au centre du quadrilatère rouge)

| \\rAccès :
| \\r

-  Tram 81 & 83 Bus 36, 59, 60, 95 (3/6 min)
-  Train par la gare Bruxelles/Luxembourg (20 min) ou bus 36 ou la gare
   d'Etterbeek (15 min) ou bus 95

| \\rBières spéciales € 2.5 Jupiler € 1.4
| Au plaisir de vous y voir

.. raw:: html

   </p>

