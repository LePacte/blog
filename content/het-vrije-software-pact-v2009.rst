Het vrije software pact v2009
#############################
:date: 2009-06-05 04:32
:slug: het-vrije-software-pact-v2009
:status: published

Dit document is ook te downloaden in PDF-formaat op [het Vrije Software
pact 2009 in pdf
\|\\rhttp://candidatsbe.april.org/public/Vrije\_Software\_Pact\_NL\_be.pdf
\| nl] of odt bewerkbare versie op `het Vrije Software pact 2009 in
odt <\"http://candidatsbe.april.org/public/Vrije_Software_Pact_NL_be.odt\">`__
met `de vrije en gratis Office-suite
OpenOffice.org <\"http://nl.openoffice.org\">`__.

\\r

Een gemeenschappelijk goed beschermen en ontwikkelen.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

Ik stel mij verkiesbaar in de Europese of de regionale parlementsverkiezingen van 2009
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

Ik ben mij er van bewust dat
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

\\r

-  Het werk van zij die tot Vrije Software bijdragen een rol speelt in
   het behouden van fundamentele vrijheden in dit digitale tijdperk, in
   het delen van kennis en in het verlagen van de drempel voor deelname
   aan de informatiemaatschappij. Dit werk schept ook mogelijkheden voor
   het algemene publiek en voor onze technologische onafhankelijkheid en
   concurrentiekracht, zowel in België, zijn gewesten en gemeenschappen,
   als in Europa;

\\r

-  Vrije Software een gemeenschappelijk goed is dat beschermd en
   ontwikkeld dient te worden. Het bestaan ervan berust op het recht van
   een auteur om diens software samen met de broncode openbaar te maken,
   en om iedereen het recht toe te kennen die software te gebruiken, te
   kopiëren, aan te passen en te verspreiden, in de oorspronkelijke of
   een gewijzigde vorm.

\\r

Daarom zal ik mij inzetten om
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

\\r

-  Alle overheden, alle publieke of lokale diensten aan te moedigen om
   de voorkeur te geven aan Vrije Software en open standaarden bij hun
   keuzes, aankopen of eigen ontwikkeling;

\\r

-  Actieve initiatieven ten voordele van Vrije Software te ondersteunen,
   en in te gaan tegen elke discriminatie ervan;

\\r

-  Op te komen voor de rechten van de auteurs en gebruikers van Vrije
   Software, in het bijzonder door de aanpassing te eisen van elke
   wettekst die deze rechten aantast, en mij te verzetten tegen elk
   wetgevend initiatief dat tot zulke aantasting zou leiden.

\\r

**Naam :**

\\r

**Voornaam:**

\\r

Verkiezing in het: Europees / Vlaams / Waals / Brussels / Duitstalig
Parlement (schrappen wat niet past).

\\r

Lijst / partij en positie op de lijst :

\\r

Persoonsgegevens:

\\r

Adres:

\\r

Postcode:

\\r

Gemeente:

\\r

Telefoonnummer:

\\r

E-mailadres:

\\r

Fax:

\\r

Getekend te:

\\r

op:

\\r

Handtekening:

\\r

Terug te sturen per post of per fax naar: April c/o Nicolas Pettiaux,
Perulaan 29 – 1000 Brussel.\\rTel : +32 496 24 55 01 – Fax : + 33 1 45
65 32 90

\\r

U kunt ook het getekende Pact inscannen en naar `pacte \_\_at\_\_
candidats.be <\"mailto:%70%61%63%74%65%20%5f%5f%61%74%5f%5f%20%63%61%6e%64%69%64%61%74%73%2e%62%65\">`__
sturen.

.. raw:: html

   </p>

