COMMUNIQUÉ DE PRESSE - Les présidents des quatre grands partis francophones s'engagent pour le logiciel, les données et l'Internet libres.
##########################################################################################################################################
:date: 2010-06-04 07:17
:category: Legislatives 2010
:slug: communique-de-presse-les-presidents-des-quatre-grands-partis-francophones-sengagent-pour-le-logiciel-les-donnees-et-linternet-libres
:status: published

.. raw:: html

   <div id="\"magicdomid6246\"" class="\"ace-line\"">

COMMUNIQUÉ DE PRESSE - Les présidents des quatre grands partis
francophones s'engagent pour le logiciel, les données et l'Internet
libres.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6248\"" class="\"ace-line\"">

BRUXELLES - le 4 juin 2010.  - LePacte.be - HetPact.be

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6250\"" class="\"ace-line\"">

Pour les élections du 13 juin 2010, les présidents des quatre grands
partis francophones (le CdH, Ecolo, le MR et le PS) s'engagent pour une
société numérique belge plus ouverte. Ils ont signé les pactes du
Logiciel Libre, de l'Internet Libre et des données libres.

.. raw:: html

   </div>

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6244\"" class="\"ace-line\"">

Ce 4 juin, trois pactes concernant\\rles libertés numériques seront
envoyés aux autres candidats pour que\\rceux-ci en fassent de  même en
signant et en adoptant les engagements\\rde ces trois textes. Coordonnés
et mis en oeuvres par l'ASBL « À l'ère\\rlibre » et avec la
participation de diverses associations qui partagent\\rcet esprit de
liberté, ces pactes mettent en évidence la nécessité de\\rreconnaître et
de défendre les valeurs fondamentales à l'ère du\\rnumérique.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6254\"" class="\"ace-line\"">

« *La signature des présidents des quatre grands partis\\rfrancophones
(le CdH, Ecolo, le MR et le PS) marque leur attachement\\raux valeurs et
intentions défendues dans ces pactes et donne un signal\\rfort aux
candidats de leurs partis d'également endosser ces engagements* » a
déclaré Nicolas Pettiaux, président de « À l'ère libre »\\ret membre du
conseil d'administration de l'April.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6256\"" class="\"ace-line\"">

         **Le\\rPacte du Logiciel Libre** est une initiative
de\\rl'April, association francophone de promotion et défense\\rdu
logiciel libre. Il a été décliné en Belgique en 2009 pour
les\\rélections communautaires et européennes dans le cadre de la
campagne\\rcandidats.be. Il met l'accent sur la nécessité d'encourager,
par des\\rmoyens institutionnels, les collectivités, les administrations
et\\rétablissements publics à développer et utiliser prioritairement
des\\rlogiciels libres et des standards ouverts. De plus, il les
encourage à\\rdéfendre les droits des auteurs et des utilisateurs de
logiciels libres.\\rIl demande notamment la modification de toute
disposition légale\\rfragilisant ces droits et en s'opposant à tout
projet ou proposition qui\\rirait dans ce sens.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6258\"" class="\"ace-line\"">

         **Le Pacte de l'Internet\\rLibre**relaye un thème lancé par
Nurpa et\\rHadopi Mayonnaise. Ce pacte rappelle que l'accès à Internet
est devenu\\rnécessaire pour l'exercice de droits fondamentaux de tout
citoyen tels\\rla liberté d'expression. Par conséquent, il est important
de garantir\\rque chacun puisse accéder ou mettre à disposition des
contenus, services\\ret applications de son choix dans le respect du
droit d'auteur. Le\\rPacte souligne également que nul ne peut contrôler,
surveiller ou\\rlimiter l'accès Internet d'un individu sans que cela ne
fasse l'objet\\rd'une procédure judiciaire équitable.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6260\"" class="\"ace-line\"">

        **Le Pacte des Données Libres** engage ses signataires à
défendre les données dites libres :\\rcelles fondées sur des licences
qui soutiennent les droits des\\rutilisateurs. Ainsi, le pacte appelle à
un soutien des actions\\rgouvernementales visant à rendre les données
non nominatives détenues\\rpar l'État enfin libres et accessibles à tous
et ce dans des formats\\rstandards et ouverts . Les conséquences
logiques seront la production de\\rdavantage de données ré-utilisables
et l'assurance que l'ensemble des\\rcitoyens puisse bénéficier de ces
données. Enfin, les signataires de ce\\rPacte affichent ainsi leur
soutien à la numérisation et la diffusion des\\rœuvres du domaine public
et souhaitent encourager les initiatives\\rcitoyennes de production de
contenus librement partagés.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6262\"" class="\"ace-line\"">

L'ensemble des trois Pactes vise à\\raccompagner l'émergence d'une
société à l'ère du numérique respectueuse\\rdes droits des individus, de
la vie privée. Il encourage également les\\rinitiatives citoyennes à
participer à la vie de la cité, notamment en\\rsoutenant les
collaborations et échanges ouverts.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6264\"" class="\"ace-line\"">

 Le décompte des signatures est\\rdisponible en temps réel
sur`http://lepacte.be <\"../\">`__.

.. raw:: html

   </div>

| 

.. raw:: html

   <div id="\"magicdomid6268\"" class="\"ace-line\"">

         **À\\rl'ère libre**est une ASBL belge qui a pour\\robjet
d’engager toute action susceptible d’assurer la promotion,
le\\rdéveloppement, la recherche et la démocratisation de l’informatique
et\\rdes œuvres libres, des formats et des protocoles ouverts. Elle
vise\\régalement à fédérer les acteurs concernés, afin notamment
d’encourager\\rle partage des connaissances et le développement d'une
informatique\\raccessible au plus grand nombre.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6269\"" class="\"ace-line\"">

Site Web :`http://alerelibre.be <\"http://alerelibre.be/\">`__
-`http://lepacte.be <\"../\">`__
-`http://hetpact.be <\"http://hetpact.be/\">`__

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6270\"" class="\"ace-line\"">

Contact presse :\\rNicolas PETTIAUX, président,
nicolas.pettiaux@alerelibre.be, +32 (0) 496\\r24 55 01 

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6271\"" class="\"ace-line\"">

          

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6272\"" class="\"ace-line\"">

         **L'April**, pionnière du logiciel libre en France, est depuis
1996 un\\racteur majeur de la démocratisation et de la diffusion du
Logiciel Libre\\ret des standards ouverts auprès du grand public, des
professionnels et\\rdes institutions dans l'espace francophone. Elle
veille aussi, dans\\rl'ère numérique, à sensibiliser l'opinion sur les
dangers d'une\\rappropriation exclusive de l'information et du savoir
par des intérêts\\rprivés. L'association est constituée de plus de 5 500
membres\\rutilisateurs et producteurs de logiciels libres.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6273\"" class="\"ace-line\"">

Site Web :`http://april.org <\"http://april.org/\">`__
-`http://candidats.fr <\"http://candidats.fr/\">`__
-`http://candidats.be <\"http://candidats.be/\">`__

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6274\"" class="\"ace-line\"">

Contact presse :\\rFrédéric COUCHET, délégué général, fcouchet@april.org
+33 (0) 6 60 68 89\\r31

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6276\"" class="\"ace-line\"">

         La**Net Users' Rights Protection Association** est un
groupement citoyen belge dédié à la défense des droits\\ret libertés des
citoyens sur Internet. Elle promeut la vision d'un\\rInternet neutre,
libre, accessible et vecteur de progrès, fidèle aux\\rvaleurs qui ont
présidé au développement de ce réseau au formidable\\rpotentiel. À ce
titre, elle intervient notamment dans les débats\\rtouchant à la liberté
d'expression, à la neutralité du Net, au droit\\rd'auteur ou encore au
respect de la vie privée.

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6277\"" class="\"ace-line\"">

Site Web :`http://nurpa.be <\"http://nurpa.be/\">`__ - Contact :
contact@nurpa.be

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6278\"" class="\"ace-line\"">

Porte-parole : Daniel FAUCON. Tel :\\r+32 (0) 487 898 774

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6280\"" class="\"ace-line\"">

         Le collectif «**Hadopi\\rMayonnaise** »  est une initiative
citoyenne\\rdont le but est d'éclairer le monde politique et les
citoyens belges sur\\rles risques liés à l'émergeance d'une loi « à la
HADOPI » française en\\rBelgique. 

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6281\"" class="\"ace-line\"">

Site Web
:`http://hadopimayonnaise.be <\"http://hadopimayonnaise.be/\">`__ -
Contact : info@hadopimayonnaise.be

.. raw:: html

   </div>

.. raw:: html

   <div id="\"magicdomid6282\"" class="\"ace-line\"">

Porte-parole :\\rRaphael WENRIC. Tel : +32 (0) 479 247 865

.. raw:: html

   </div>

.. raw:: html

   </p>

