L'Europe pousse les gouvernements vers le logiciel libre
########################################################
:date: 2010-06-11 06:07
:category: Free Software
:slug: leurope-pousse-les-gouvernements-vers-le-logiciel-libre
:status: published

Selon le quotidien français `Le Monde <\"http://lemonde.fr/\">`__: *La
commissaire européenne en charge du numérique,* `Neelie
Kroes <\"http://www.lemonde.fr/sujet/6f5c/neelie-kroes.html\">`__\ *, a
mis en garde ce jeudi les gouvernements européens contre le risque de se
voir *\\"enfermés accidentellement dans une technologie
propriétaire\\"*, visant sans le nommer Microsoft, et annoncé la mise en
place d'un\\rprogramme de conseils aux Etats pour qu'ils développent
leur utilisation de logiciels libres.*

Dans un `article de ce jeudi 10 juin 2010 à
18h, <\"http://www.lemonde.fr/technologies/article/2010/06/10/l-europe-pousse-les-gouvernements-vers-le-logiciel-libre_1370844_651865.html\">`__
le grand quotidien français `Le Monde <\"http://lemonde.fr\">`__
poursuit

\\r *Dans* `son
discours <\"http://europa.eu/rapid/pressReleasesAction.do?reference=SPEECH/10/300&format=HTML&aged=0&language=EN&guiLanguage=en\">`__
*à l'*\ `Openforum
Europe <\"http://www.lemonde.fr/sujet/4234/openforum-europe.html\">`__\ *,\\rune
association de promotion du logiciel libre, la commissaire\\reuropéenne
a souligné que les technologies propriétaires pouvaient à\\rterme
devenir tellement prévalentes *\\"que les alternatives sont
systématiquement ignorées\\"*, alors même qu'il peut exister des
solutions équivalentes libres et gratuites. *\\"C'est un gaspillage
d'argent public que la plupart des administrations ne peuvent plus se
permettre\\"*, juge Neelie Kroes.*\\r

*Précédemment commissaire européenne à la concurrence, Neelie
Kroes\\ravait lancé plusieurs enquêtes contre Microsoft, qui ont abouti
à la\\rcondamnation du géant du logiciel à des amendes record, pour son
refus\\rde partager certaines informations avec ses rivaux et
pour\\rl'installation par défaut d'\ `Internet
Explorer <\"http://www.lemonde.fr/sujet/aa62/internet-explorer.html\">`__
comme navigateur Web dans Windows.*

\\r

*Une problématique similaire s'est posée récemment outre-Atlantique : la
semaine dernière, la `justice
québecoise <\"http://www.lemonde.fr/technologies/article/2010/06/04/victoire-judiciaire-pour-le-logiciel-libre-au-quebec_1368034_651865.html\">`__\\ra
conclu que les autorités de la province canadienne avaient
agi\\rillégalement en achetant des logiciels Microsoft sans lancer
une\\rprocédure d'appel d'offre, parce qu'il existait des concurrents
libres\\ret gratuits.*

\\r

Voici des propos qui vont dans le même sens que ceux défendus dans les
pactes des libertés numériques, et en particulier le pacte du logiciel
libre. Nous pensons que pour qu'une mise en oeuvre efficace puisse avoir
lieu, il faut aussi une prise de conscience des dirigeants au niveaux
des états, et des actions concrètes par ceux-ci. Nous sommes là pour les
aider ... avec le soutien des citoyens concernés.

.. raw:: html

   </p>

