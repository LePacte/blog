Statistiques des signataires du pacte des libertés numériques ce 14 octobre 2012 à 10h
######################################################################################
:date: 2012-10-14 08:27
:category: Communales 2012
:slug: statistiques-des-signataires-du-pacte-des-libertes-numeriques-ce-14-octobre-2012-a-10h
:status: published

+--------------------------------------+--------------------------------------+
| Indépendants                         | PS                                   |
| 2                                    | 36                                   |
+--------------------------------------+--------------------------------------+

| 
| Elles\\rconcernent les nouveaux signataires des pactes des libertés
numériques\\rou ceux qui ont déjà signé en 2009 et 2010 et pour lesquels
nous avons\\rpu identifier, avec des moyens humains très limités, qu'ils
se\\rreprésentaient aux élections communales de 2012 après les
élections\\rrégionales de 20109 et celles fédérales de 2010.
| Pour plus de détails aller sur
`http://lepacte.be/communales2012/ <\"/communales2012/>`__\ et pour
savoir si votre candidat favori a signé, sur `cette
page <\"/communales2012/?action=liste_signataires&o_sens=DESC&o_date=1\">`__
| \\r
| Attention, ces nombres contiennent aussi des promesses
de\\rsignatures, c'est à dire des encodages pour lesquels nous n'avons
pas\\rencore reçu la confirmation par courrier électronique ou la
signature\\ravec la carte d'identité électronique.

.. raw:: html

   </p>

