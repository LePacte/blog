16 candidats signataires et encore un peu de pagaille
#####################################################
:date: 2009-06-01 20:16
:category: Européennes et régionales 2009
:slug: 16-candidats-signataires-et-encore-un-peu-de-pagaille
:status: published

Ce soir du lundi 1 juin 2009, nous avons 16 signataires dont un ministre
bruxellois sortant.

La diversité de partis augmente encore. Les rencontres personnelles ont
été efficaces, ainsi que des contacts par courrier électronique parfois
suivi de conversations téléphoniques.

Mais il reste un peu de pagaille par rapport aux collèges électoraux de
chaque candidat. Nous travaillerons à régler cela demain si possible.

.. raw:: html

   </p>

