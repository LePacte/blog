Votez et soyez informés des positions des partis et des candidats sur les libertés numériques
#############################################################################################
:date: 2010-06-13 05:59
:category: Legislatives 2010
:slug: votez-et-soyez-informes-des-positions-des-partis-et-des-candidats-sur-les-libertes-numeriques
:status: published

Le matin du scrutin, sans doute historique pour la Belgique, voici un
premier bilan des actions lepacte.be / hetpact.be, pour que vous
puissiez voter en connaissance de cause, informés des positions des
partis et des candidats sur les libertés numériques.

\\r

D'abord, cette campagne a été \ **un succès:** **194 candidats ont
signé** dont les \ **quatre présidents des grands partis francophones,
le président de Groen!, le 3è effectif et la première suppléante du SP-a
au Sénat, les présidents de ProBruxsel et du parti pirate**. Un large
consensus donc autour de ces questions des libertés numériques. **
**

\\r

**Nous le remercions tous et leur souhaitons bonne chance à tous.**

\\r

.. raw:: html

   <div style="\&quot;color:" rgb(0,="" 0,="" 0);=""
   background-color:="" rgb(249,="" 249,="" 249);="" margin:="" 0;=""
   padding:="" 2px;="" border-style:="" none;\"="">

| Vous pouvez connaître les positions nominativement sur la \ `liste des
signataires <\"http://lepacte.be/legis2010/index.php?action=liste_signataires&o_additional_position_in_list=1&o_sens=ASC#table_candidats\">`__
| où vous pouvez trier et chercher les candidats qui vous intéressent.

\\r

.. raw:: html

   </div>

\\r

Comme le titrait un journal important, \ *`Le PS, le MR, le cdH et Ecolo
sont au moins d’accord sur un point : il faut défendre les «libertés
numériques». <\"http://trends.rnews.be/fr/economie/actualite/high-tech/les-4-grands-partis-francophones-signent-un-triple-pacte-internet/article-1194747967008.htm#\">`__ *\ Comme
ces libertés sont aussi soutenues par plusieurs ténors du nord du pays,
nous pouvons espérer et nous attendre donc que des points liés aux
engagements pris en signant ces pactes soient effectivement présents
dans les déclarations gouvernementales. 

Avec plus de détails et par ordre alphabétique des grands partis
francophones (les analyses des programmes sont reprises des analyses
précédentes et présentées ici pour la simplicité de la consultation;
voir les liens respectifs \ `pour les
données <\"/?post/2010/05/31/Les-donn%C3%A9es-gouvernementales-dans-les-programmes-des-partis\">`__ et
`pour les
logiciels <\"http://www.candidats.be/index.php?post/2010/05/31/Dans-les-programmes-des-partis-II\">`__)

\\r

cdH
===

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

70 candidats,

.. raw:: html

   </li>

\\r

.. raw:: html

   <li>

13 têtes de listes et leur suppléants (sur un maximum de 14)

.. raw:: html

   </li>

\\r

.. raw:: html

   <li>

*dans le programme (`5ème
partie <\"http://www.lecdh.be/sites/default/files/partie5-2pages_0.pdf\">`__),
pour les données*,  le cdH propose de

.. raw:: html

   <ul>

.. raw:: html

   <li>

| Garantir l’information du citoyen
| L’information du citoyen est à la fois un droit fondamental, une
garantie d’efficacité et une garantie de l’inclusion de tous dans le
processus politique. Elle constitue un élément essentiel de la réforme
de la gouvernance. Elle permet ainsi d’améliorer la confiance que les
citoyens peuvent avoir envers leurs institutions.
| a. Utiliser les méthodes multimédia pour fournir l’information 
| Le cdH propose de :

.. raw:: html

   </li>

\\r

-   Etablir une politique de diffusion systématique des documents sur
   les sites Internet fédéraux, notamment de tous les documents
   préparatoires à une décision ainsi que les rapports extérieurs
   réalisés au profit des autorités, dans un souci de transparence.

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </li>

\\r

.. raw:: html

   <li>

*dans le programme (`première
partie <\"http://www.lecdh.be/sites/default/files/partie1-2pages_5.pdf\">`__),
pour les logiciels*, le cdH propose de

.. raw:: html

   </li>

\\r

-  Favoriser l'utilisation des logiciels libres que ce soit au niveau
   des configurations préinstallées ou au niveau de l'utilisation par
   les pouvoirs publics ;

\\r

.. raw:: html

   </ul>

\\r

Ecolo
=====

\\r

.. raw:: html

   <div>

-  62 candidats,
-  14 têtes de listes et leurs suppléants (tous, sur un maximum de 14)
-  *dans le programme (partie `Démocratie - Ethique
   politique <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Democratie_Ethique_politique.pdf\">`__),
   pour les données*,  Ecolo propose d'

   -  Assurer au citoyen l’accès à l’information 
      Le développement de la participation citoyenne repose sur un
      pré-requis, à savoir que chacun et chacune ait un libre accès aux
      décisions publiques. En d’autres termes, pour être en mesure de
      participer à la décision publique, il faut pouvoir disposer de
      l’information. Le droit à pouvoir consulter les actes
      administratifs et, en particulier, les actes préparatoires à la
      décision finale de l’autorité publique a beau être consacré dans
      notre Constitution (article 32) ou dans des instruments de droit
      international ou européen (Convention d’Aarhus), il n’en demeure
      pas moins sujet à difficultés et tracasseries sur le terrain et
      ce, aux différents niveaux de pouvoir.
      Par conséquent, Ecolo propose de systématiser une publicité active
      des actes administratifs, en ce compris celle des avis et actes
      préparatoires aux décisions publiques (avis du Conseil d’Etat et
      d’autres instances de consultation) et de développer les supports
      de la communication à cet égard.

   \\r
-  *pour les logiciels*, Ecolo propose de

\\r

.. raw:: html

   <div>

Programme \ `Service Public -
Gouvernance <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Service_public_Gouvernance.pdf\">`__:

-  Ecolo considère que les services publics doivent répondre à des
   exigences particulières de responsabilité sociale et soutient à ce
   titre : [...]

   -  l’utilisation de logiciels informatiques utilisant des standards
      ouverts, dans le but de garantir l’interopérabilité des systèmes,
      de sécuriser l’archivage des contenus numériques et de garantir la
      pérennité de l’accès électronique aux contenus numériques.

   \\r

\\rProgramme \ `Société de
l'Information <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Societe_de_l_information.pdf\">`__:

-  Afin que les TIC ne se transforment pas en un nouveau fossé social,
   il importe de favoriser l’accès et la participation de chacun à ces
   nouveaux médias. A cette fin, Ecolo propose notamment : [...]

   -  de maintenir l'open source, un langage technique et un trafic des
      données ouverts : quiconque doit pouvoir participer à la
      construction d’une société de l’information, sans devoir payer des
      royalties ou réinventer un langage pour communiquer avec d’autres
      systèmes d’information ; des protocoles et standards ouverts,
      labellisés par une norme publique, permettront par ailleurs
      d'intégrer plus facilement des modes d'accès adaptés aux personnes
      handicapées ; enfin, Ecolo défend le principe de neutralité du net
      ;
   -  de systématiser l’utilisation des logiciels libres de droits dans
      le champ public (pouvoirs publics, administrations, écoles …).

   \\r

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

L’exploitation des TIC représente aussi une source considérable de
profit. [...] Ecolo entend soutenir la mise en place d’un cadre de
régulation adapté permettant d’orienter le développement de ce marché en
vue d’un accès garanti à tous et d’une société de l’information durable.
Pour ce faire, Ecolo propose : [...]

.. raw:: html

   </li>

\\r

-  de permettre à chacun de choisir voire de développer ses propres
   outils logiciels ; Ecolo souhaite ainsi lutter contre la présence
   automatique de logiciels imposés par les fabricants de matériel
   informatique, en appliquant de façon plus stricte les dispositions
   visant à lutter contre les pratiques commerciales déloyales telles
   que définies dans la nouvelle loi sur les pratiques de marché ;

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </div>

.. raw:: html

   <ul>

\\r

-  Favoriser l'utilisation des logiciels libres que ce soit au niveau
   des configurations préinstallées ou au niveau de l'utilisation par
   les pouvoirs publics ;

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </div>

MR
==

\\r

.. raw:: html

   <div>

.. raw:: html

   <div>

-  12 candidats,
-  4 têtes de listes et leur suppléants ( sur un maximum de 14)
-  *dans le programme (`Programme
   2010 <\"http://www.mr.be/media/images/content/Programme2010.pdf\">`__) pour
   les données*,  le MR propose de quelques notions de transparence de
   l'administration mais rien de très clair (cf Fonction Publique)
-  *pour les logiciels*, le MR ne fait aucune mention des logiciels
   libres ou des standards ouverts

\\r
PS
==

\\r

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div>

-  32 candidats,
-  11 têtes de listes et leur suppléants ( sur un maximum de 14)

\\r

.. raw:: html

   <div>

.. raw:: html

   <ul>

.. raw:: html

   <li>

*dans le programme (`Programme
2010 <\"http://www.ps.be/_iusr/programme_2010___version_finale.pdf\">`__ pour
les données*,  le PS propose de

.. raw:: html

   <ul>

.. raw:: html

   <li>

Réduire la fracture numérique: 
Dans l’esprit du Plan National de lutte contre la fracture numérique,
porté par nos ministres sous les précédentes législatures, le PS lutte
contre l’émergence d’une société de l’information à deux vitesses en
levant les obstacles financiers, culturels, sociologiques et éducatifs
qui empêchent un accès libre et égal de tous les citoyens aux nouvelles
technologies. Concrètement, pour atteindre ces objectifs, le PS propose
de : [...]

.. raw:: html

   </li>

\\r

-  favoriser l’émergence d’un domaine public de qualité notamment par la
   numérisation et la mise en ligne des archives des institutions
   culturelles ou scientifiques fédérales ;

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

Garantir l’information du citoyen
L’information du citoyen est à la fois un droit fondamental, une
garantie d’efficacité et une garantie de l’inclusion de tous dans le
processus politique. Elle constitue un élément essentiel de la réforme
de la gouvernance. Elle permet ainsi d’améliorer la confiance que les
citoyens peuvent avoir envers leurs institutions.
a. Utiliser les méthodes multimédia pour fournir l’information 
Le cdH propose de :

.. raw:: html

   </li>

\\r

-   Etablir une politique de diffusion systématique des documents sur
   les sites Internet fédéraux, notamment de tous les documents
   préparatoires à une décision ainsi que les rapports extérieurs
   réalisés au profit des autorités, dans un souci de transparence.

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </li>

\\r

.. raw:: html

   <li>

*dans le programme (`Programme
2010 <\"http://www.ps.be/_iusr/programme_2010___version_finale.pdf\">`__),
pour les logiciels*, le PS propose de

.. raw:: html

   <ul>

.. raw:: html

   <li>

Réduire la fracture numérique [...]

.. raw:: html

   </li>

\\r

-  encourager l'utilisation de standards ouverts et des logiciels libres
   pour garantir un accès libre des citoyens à tous les contenus en
   ligne.

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </li>

\\r

-  Favoriser l'utilisation des logiciels libres que ce soit au niveau
   des configurations préinstallées ou au niveau de l'utilisation par
   les pouvoirs publics ;

\\r

.. raw:: html

   </ul>

\\r

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </p>

