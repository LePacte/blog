Le Pacte du Logiciel Libre - Élections communales 2012
######################################################
:date: 2012-10-07 12:35
:slug: le-pacte-du-logiciel-libre-elections-communales-2012
:status: published

Ce document est également téléchargeable en version pdf ici: `Pacte du
Logiciel Libre 2012
(pdf) <\"public/Le_pacte_du_logiciel_libre_2012.pdf\">`__.

\\r

Vous pouvez donc soit `signer le pacte de manière électronique et sûre
sur notre site <\"https://jesigne.lepacte.be/\">`__, soit remplir le
formulaire ici, imprimer cette page web, la signer et nous la renvoyer
par courrier, fax ou (scanné) par email, ou alors remplir le formulaire
du pdf, imprimer le pdf, le signer et nous le renvoyer.

\\r

\\r\ **LE\\rPACTE DU LOGICIEL LIBRE**

\\r

**Un\\rbien commun à protéger et à développer**

\\r

**Je\\rsuis candidat(e) aux élections communales 2012**

\\r

**J’ai\\rconscience que**

\\r

\\rLe\\rtravail des acteurs du Logiciel Libre participe à la
préservation\\rdes libertés fondamentales à l’ère du numérique, au
partage du\\rsavoir et à la lutte contre la « fracture numérique ».\\rIl
constitue également une opportunité pour le public mais aussi\\rpour
l’indépendance technologique et la compétitivité de nos\\rrégions, de
nos communautés, de la Belgique et de l’Europe.

\\r

\\rLe\\rLogiciel Libre est un bien commun à protéger et à
développer.\\rSon\\rexistence repose sur le droit pour un auteur de
divulguer son\\rlogiciel avec son code source et d’accorder à tous le
droit de les\\rutiliser, les copier, les adapter et les redistribuer, en
version\\roriginale ou modifiée.

\\r

\\rL’usage\\rgénéralisé de standards ouverts permet à tous
l’accès,\\rl’échange et la pérennité de l’information.

\\r

**Je\\rm’engage donc à**

\\r

\\rEncourager par\\rdes moyens institutionnels les administrations
publiques,\\rétablissements publics et collectivités à développer et
utiliser\\rprioritairement des logiciels libres et des standards
ouverts.

\\r

\\rSoutenir des\\rpolitiques actives en faveur du Logiciel Libre et
m’opposer à toute\\rdiscrimination à son encontre.

\\r

\\rDéfendre les\\rdroits des auteurs et des utilisateurs de logiciels
libres, notamment\\ren demandant la modification de toute disposition
légale fragilisant\\rces droits et en m’opposant à tout projet ou
proposition qui irait\\rdans ce sens.

\\r

.. raw:: html

   <form name="\&quot;Form\&quot;">

\\r

.. raw:: html

   <div align="\&quot;JUSTIFY\&quot;">

\\r
\\rNom :\\r\\rPrénom : \\rParti :

\\r
Adresse :\\r

\\r
Code\\rpostal : \\rVille :

\\r
| Courriel :\\r\\rTel :
| Fait\\rle : \\rÀ :

\\r
Signature :

\\r

.. raw:: html

   </div>

\\r

.. raw:: html

   </form>

\\r

\\rÀ renvoyer par\\rcourrier, fax ou (scanné) par email à :

\\r

\\r\ **Nicolas\\rPettiaux**

\\r

\\r\ **Avenue du\\rPérou 29 - 1000 Bruxelles**

\\r

\\r\ **Fax :\\r02 350 12 66 - email :
`jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**

\\r

\\r\ **Contact :\\r\ `info@lepacte.be <\"mailto:info@lepacte.be\">`__ ou
gsm 0496 24\\r55 01**

| \\r

***Document\\rde l’initiative belge
`Candidats.be <\"http://candidats.be/\">`__\\rde
l’\ `April <\"http://april.org/\">`__\\rsoutenue par l’a.s.b.l.
`À\\rl’Ère Libre <\"http://www.alerelibre.be/\">`__*** \\r

.. raw:: html

   </p>

