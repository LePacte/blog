Modèles de lettres au candidats
###############################
:date: 2012-10-11 04:24
:category: Communales 2012
:slug: modeles-de-lettres-au-candidats
:status: published

Le vote est sans doute l'acte démocratique le plus important ... et ne doit donc pas être pris à la légère. Faites savoir aux candidats ce qui est important pour vous.
=======================================================================================================================================================================

| \\rMerci de vous inspirer des courriers ci-dessous pour vous adresser
aux candidats de votre commune et les inviter à signer le pacte des
liberté numériques.
| Il y a un modèle les candidats de tout partis, et un spécifique pour
les candidats des 4 grands partis francophones (MR, cdH, PS et Ecolo).
Un petit coup de copier-coller et cela fera l'affaire.
| N'hésitez pas à adapter, personnaliser, mettre vos coordonnées
téléphoniques pour que les candidats, souvent de bonne volonté mais
comme la plupart d'entre nous, peu versés en matière technologiques
puissent vous contacter et avoir des informations, savoir pourquoi ces
questions sont importantes pour vous, et pourquoi ils devraient donc
s'en préoccuper et les relayer dans les niveaux de pouvoir qu'ils
sollicitent.
| Vous pouvez bien sûr aussi aller au marché, aux rencontres qu'ils
proposent ... pour leur soumettre le `pacte des libertés
numériques <\"https://jesigne.lepacte.be/data/lepacte-be_des_libertes_numeriques_2012.pdf\">`__,
le faire compléter et signer, puis remplir vous-même pour eux le
formulaire
**`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\ret\\rtransmettre
le formulaire papier scanné ou photographié par **email à
`jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.
| N'oubliez pas qu'ils **vous représentent** et que c'est le moment où
jamais de leur faire savoir ce qui est **important pour vous**, pourquoi
vous les choisissez plutôt que les autres candidats.

Pour tous les candidats
=======================

\\r

.. raw:: html

   <div>

**Concerne** : Invitation à signer les 3 pactes des libertés numériques

.. raw:: html

   </div>

.. raw:: html

   <div>

Mesdames, Messieurs,

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Chers candidats aux élections communales du 14 octobre prochain,

.. raw:: html

   </div>

.. raw:: html

   <div>

Internet,\\rl'ordinateur, les téléphones portables et en fait tous les
objets de la\\rvie quotidienne, y compris l'auto et le lave vaisselle,
utilisent de\\rplus en plus les technologies numériques.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Vous êtes donc sûrement, comme les citoyens\\rque nous sommes, sensibles
à l'emprise de ces technologies sur nos vies,\\ret soucieux du respect
des libertés numériques qui reflètent des\\rvaleurs que nous devons
préserver.
\\r

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

\\rNous avons essayé de traduire quelques-unes de celles-ci dans les
3\\rpactes des libertés numériques, qui déclinent 3 thèmes qui nous
sont\\rchers. Ils concernent respectivement les données libres,
l'internet\\rlibre et le logiciel libre.
Nous les soumettons à votre signature, comme nous les avons soumis aux
présidents des 4 partis francophones imporants et au premier ministre
qui\\rles ont signés tous les 3, comme plus de 200 candidats et 37 élus
francophones l'ont\\rfait lors du scrutin de 2010. (voir aussi la
`listes des signataires de
2010 <\"http://lepacte.be/?post/2012/10/10/Les-signataires-des-pactes-principaux-en-2010\">`__).
Cette version 2012 est absolument identique à celle de 2012, moyennent
bien une adaptation au type de scrutin.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Pour votre information, nous suivons dans l'outil
`**http://lepacte.be** <\"http://lepacte.be\">`__\\ret en particulier le
lien en haut à gauche «Résultats de campagne ?»,\\ren temps réel, les
signatures que nous avons reçues, totalisés aussi par\\rparti, et nous
en informons régulièrement tant la presse que nos\\rconcitoyens pour
qu'ils puissent, le 14 octobre prochain, connaître vos\\rpositions sur
ces sujets et faire un choix éclairé. Sachez que plusieurs\\rcentaines
de personnes nous ont fait savoir que cela avait eu une\\rinfluence sur
leurs votes.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Vous trouverez le **détails des textes des 3 pactes et la procédure de
signature** à
l'adresse\ **`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\roù
vous pourrez les signer avec votre carte d'identité électronique\\rcomme
vous l'utilisez pour remplir votre déclaration d'impôt, ou, si\\rvous ne
pouvez signer électroniquement, à remplir ce même formulaire
en\\rligne,\\rl'imprimer (version pdf à droite), le signer
manuscritement et nous le\\rtransmettre scanné ou photographié par
**email à `jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous\\rsommes à votre disposition pour en discuter si vous le souhaitez.
Par\\rexemple par téléphone au coordinateur Nicolas Pettiaux `0496 24 55
01 <\"tel:0496%2024%2055%2001\">`__ ou par email à `info@lepacte.be
 <\"mailto:info@lepacte.be\">`__
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous espérons pouvoir vous\\rrencontrer un de ces jours pour discuter de
ces questions qui nous\\rtiennent à cœur et vous souhaitons, d'ici là,
une bonne dernière semaine\\rde campagne et les meilleurs résultats
possibles, et vous invitons à\\rtransmettre ce message à vos colistiers.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\r
Dans l'espoir de vivre avec vous dans un société numérique plus juste,
plus respecteuse, plus solidaire et plus libre,

.. raw:: html

   </div>

.. raw:: html

   <div>

Bien à vous,

.. raw:: html

   </div>

.. raw:: html

   <div>

<SIGNATURE>
Pour la campagne `lepacte.be <\"http://lepacte.be\">`__\ 

.. raw:: html

   </div>

Pour les candidats du centre démocrate humaniste (cdH)
======================================================

\\r

.. raw:: html

   <div>

**Concerne** : Invitation à signer les 3 pactes des libertés numériques

.. raw:: html

   </div>

.. raw:: html

   <div>

Mesdames, Messieurs,

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Chers candidats aux élections communales du 14 octobre prochain,

.. raw:: html

   </div>

.. raw:: html

   <div>

Internet,\\rl'ordinateur, les téléphones portables et en fait tous les
objets de la\\rvie quotidienne, y compris l'auto et le lave vaisselle,
utilisent de\\rplus en plus les technologies numériques.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Vous êtes donc sûrement, comme les citoyens\\rque nous sommes, sensibles
à l'emprise de ces technologies sur nos vies,\\ret soucieux du respect
des libertés numériques qui reflètent des\\rvaleurs que nous devons
préserver.
\\r

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

\\rNous avons essayé de traduire quelques-unes de celles-ci dans les
3\\rpactes des libertés numériques, qui déclinent 3 thèmes qui nous
sont\\rchers. Ils concernent respectivement les données libres,
l'internet\\rlibre et le logiciel libre.
Nous les soumettons à votre signature, comme nous les avons soumis\\ren
primeur au Président Benoît Lutgen et à la vice
première-ministre\\rJoëlle Milquet, qui\\rles ont signés tous les 3,
comme de 71 candidats et 10 élus cdH l'ont\\rfait lors du scrutin de
2010. (voir aussi la `listes des signataires de
2010 <\"http://lepacte.be/?post/2012/10/10/Les-signataires-des-pactes-principaux-en-2010\">`__).
Cette version 2012 est absolument identique à celle de 2012, moyennent
bien une adaptation au type de scrutin.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Pour votre information, nous suivons dans l'outil
`**http://lepacte.be** <\"http://lepacte.be\">`__\\ret en particulier le
lien en haut à gauche «Résultats de campagne ?»,\\ren temps réel, les
signatures que nous avons reçues, totalisés aussi par\\rparti, et nous
en informons régulièrement tant la presse que nos\\rconcitoyens pour
qu'ils puissent, le 14 octobre prochain, connaître vos\\rpositions sur
ces sujets et faire un choix éclairé. Sachez que plusieurs\\rcentaines
de personnes nous ont fait savoir que cela avait eu une\\rinfluence sur
leurs votes.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Vous trouverez le **détails des textes des 3 pactes et la procédure de
signature** à
l'adresse\ **`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\roù
vous pourrez les signer avec votre carte d'identité électronique\\rcomme
vous l'utilisez pour remplir votre déclaration d'impôt, ou, si\\rvous ne
pouvez signer électroniquement, à remplir ce même formulaire
en\\rligne,\\rl'imprimer (version pdf à droite), le signer
manuscritement et nous le\\rtransmettre scanné ou photographié par
**email à `jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous\\rsommes à votre disposition pour en discuter si vous le souhaitez.
Par\\rexemple par téléphone au coordinateur Nicolas Pettiaux `0496 24 55
01 <\"tel:0496%2024%2055%2001\">`__ ou par email à `info@lepacte.be
 <\"mailto:info@lepacte.be\">`__
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous espérons pouvoir vous\\rrencontrer un de ces jours pour discuter de
ces questions qui nous\\rtiennent à cœur et vous souhaitons, d'ici là,
une bonne dernière semaine\\rde campagne et les meilleurs résultats
possibles, et vous invitons à\\rtransmettre ce message à vos colistiers.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\r
Dans l'espoir de vivre avec vous dans un société numérique plus juste,
plus respecteuse, plus solidaire et plus libre,

.. raw:: html

   </div>

.. raw:: html

   <div>

Bien à vous,

.. raw:: html

   </div>

.. raw:: html

   <div>

<SIGNATURE>
Pour la campagne `lepacte.be <\"http://lepacte.be\">`__\ 

.. raw:: html

   </div>

Pour les candidats du Parti Socialiste (PS)
===========================================

\\r

.. raw:: html

   <div>

**Concerne** : Invitation à signer les 3 pactes des libertés numériques

.. raw:: html

   </div>

.. raw:: html

   <div>

Mesdames, Messieurs,

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Chers candidats aux élections communales du 14 octobre prochain,

.. raw:: html

   </div>

.. raw:: html

   <div>

Internet,\\rl'ordinateur, les téléphones portables et en fait tous les
objets de la\\rvie quotidienne, y compris l'auto et le lave vaisselle,
utilisent de\\rplus en plus les technologies numériques.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Vous êtes donc sûrement, comme les citoyens\\rque nous sommes, sensibles
à l'emprise de ces technologies sur nos vies,\\ret soucieux du respect
des libertés numériques qui reflètent des\\rvaleurs que nous devons
préserver.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\rNous avons essayé de traduire quelques-unes de celles-ci dans les
3\\rpactes des libertés numériques, qui déclinent 3 thèmes qui nous
sont\\rchers. Ils concernent respectivement les données libres,
l'internet\\rlibre et le logiciel libre.xxx

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

.. raw:: html

   <div>

Nous les soumettons à votre signature, comme nous les avons\\rsoumis en
primeur au Président Thierry Giet et au Premier ministre Elio\\rDi
Rupo,qui\\rles ont signés tous les 3, comme de 35 candidats et 15
élus\\rsocialistes l'ont fait lors du scrutin de 2010. (voir aussi la
`listes des signataires de
2010 <\"http://lepacte.be/?post/2012/10/10/Les-signataires-des-pactes-principaux-en-2010\">`__).
Cette version 2012 est absolument identique à celle de 2012, moyennent
bien une adaptation au type de scrutin.
\\r

.. raw:: html

   </div>

\\r
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Pour votre information, nous suivons dans l'outil
`**http://lepacte.be** <\"http://lepacte.be\">`__\\ret en particulier le
lien en haut à gauche «Résultats de campagne ?»,\\ren temps réel, les
signatures que nous avons reçues, totalisés aussi par\\rparti, et nous
en informons régulièrement tant la presse que nos\\rconcitoyens pour
qu'ils puissent, le 14 octobre prochain, connaître vos\\rpositions sur
ces sujets et faire un choix éclairé. Sachez que plusieurs\\rcentaines
de personnes nous ont fait savoir que cela avait eu une\\rinfluence sur
leurs votes.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Vous trouverez le **détails des textes des 3 pactes et la procédure de
signature** à
l'adresse\ **`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\roù
vous pourrez les signer avec votre carte d'identité électronique\\rcomme
vous l'utilisez pour remplir votre déclaration d'impôt, ou, si\\rvous ne
pouvez signer électroniquement, à remplir ce même formulaire
en\\rligne,\\rl'imprimer (version pdf à droite), le signer
manuscritement et nous le\\rtransmettre scanné ou photographié par
**email à `jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous\\rsommes à votre disposition pour en discuter si vous le souhaitez.
Par\\rexemple par téléphone au coordinateur Nicolas Pettiaux `0496 24 55
01 <\"tel:0496%2024%2055%2001\">`__ ou par email à `info@lepacte.be
 <\"mailto:info@lepacte.be\">`__
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous espérons pouvoir vous\\rrencontrer un de ces jours pour discuter de
ces questions qui nous\\rtiennent à cœur et vous souhaitons, d'ici là,
une bonne dernière semaine\\rde campagne et les meilleurs résultats
possibles, et vous invitons à\\rtransmettre ce message à vos colistiers.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\r
Dans l'espoir de vivre avec vous dans un société numérique plus juste,
plus respecteuse, plus solidaire et plus libre,

.. raw:: html

   </div>

.. raw:: html

   <div>

Bien à vous,

.. raw:: html

   </div>

.. raw:: html

   <div>

<SIGNATURE>
Pour la campagne `lepacte.be <\"http://lepacte.be\">`__\ 

.. raw:: html

   </div>

\\r

Pour les candidats d'Ecolo
==========================

\\r

.. raw:: html

   <div>

**Concerne** : Invitation à signer les 3 pactes des libertés numériques

.. raw:: html

   </div>

.. raw:: html

   <div>

Mesdames, Messieurs,

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Chers candidats aux élections communales du 14 octobre prochain,

.. raw:: html

   </div>

.. raw:: html

   <div>

Internet,\\rl'ordinateur, les téléphones portables et en fait tous les
objets de la\\rvie quotidienne, y compris l'auto et le lave vaisselle,
utilisent de\\rplus en plus les technologies numériques.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Vous êtes donc sûrement, comme les citoyens\\rque nous sommes, sensibles
à l'emprise de ces technologies sur nos vies,\\ret soucieux du respect
des libertés numériques qui reflètent des\\rvaleurs que nous devons
préserver.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\rNous avons essayé de traduire quelques-unes de celles-ci dans les
3\\rpactes des libertés numériques, qui déclinent 3 thèmes qui nous
sont\\rchers. Ils concernent respectivement les données libres,
l'internet\\rlibre et le logiciel libre.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Nous les soumettons à votre signature, comme\\rnous les avons soumis en
primeur aux présidents Emily Hoyos et Olivier\\rDeleuze et plusieurs
ministres Ecolo, qui\\rles ont signés tous les 3, comme de 71 candidats
et 10 élus Ecolo l'ont\\rfait lors du scrutin de 2010. (voir aussi la
`listes des signataires de
2010 <\"http://lepacte.be/?post/2012/10/10/Les-signataires-des-pactes-principaux-en-2010\">`__).
Cette version 2012 est absolument identique à celle de 2012, moyennent
bien une adaptation au type de scrutin.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Pour votre information, nous suivons dans l'outil
`**http://lepacte.be** <\"http://lepacte.be\">`__\\ret en particulier le
lien en haut à gauche «Résultats de campagne ?»,\\ren temps réel, les
signatures que nous avons reçues, totalisés aussi par\\rparti, et nous
en informons régulièrement tant la presse que nos\\rconcitoyens pour
qu'ils puissent, le 14 octobre prochain, connaître vos\\rpositions sur
ces sujets et faire un choix éclairé. Sachez que plusieurs\\rcentaines
de personnes nous ont fait savoir que cela avait eu une\\rinfluence sur
leurs votes.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Vous trouverez le **détails des textes des 3 pactes et la procédure de
signature** à
l'adresse\ **`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\roù
vous pourrez les signer avec votre carte d'identité électronique\\rcomme
vous l'utilisez pour remplir votre déclaration d'impôt, ou, si\\rvous ne
pouvez signer électroniquement, à remplir ce même formulaire
en\\rligne,\\rl'imprimer (version pdf à droite), le signer
manuscritement et nous le\\rtransmettre scanné ou photographié par
**email à `jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous sommes à votre disposition pour en discuter si vous le souhaitez.
Par exemple par téléphone au coordinateur Nicolas Pettiaux `0496 24 55
01 <\"tel:0496%2024%2055%2001\">`__ ou par email à `info@lepacte.be
 <\"mailto:info@lepacte.be\">`__
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous espérons pouvoir vous\\rrencontrer un de ces jours pour discuter de
ces questions qui nous\\rtiennent à cœur et vous souhaitons, d'ici là,
une bonne dernière semaine\\rde campagne et les meilleurs résultats
possibles, et vous invitons à\\rtransmettre ce message à vos colistiers.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\r
Dans l'espoir de vivre avec vous dans un société numérique plus juste,
plus respecteuse, plus solidaire et plus libre,

.. raw:: html

   </div>

.. raw:: html

   <div>

Bien à vous,

.. raw:: html

   </div>

.. raw:: html

   <div>

<SIGNATURE>
Pour la campagne `lepacte.be <\"http://lepacte.be\">`__\ 

.. raw:: html

   </div>

| 
| \\r

Pour les candidats du Mouvement Réformateur (MR)
================================================

\\r

.. raw:: html

   <div>

**Concerne** : Invitation à signer les 3 pactes des libertés numériques

.. raw:: html

   </div>

.. raw:: html

   <div>

Mesdames, Messieurs,

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Chers candidats aux élections communales du 14 octobre prochain,

.. raw:: html

   </div>

.. raw:: html

   <div>

Internet,\\rl'ordinateur, les téléphones portables et en fait tous les
objets de la\\rvie quotidienne, y compris l'auto et le lave vaisselle,
utilisent de\\rplus en plus les technologies numériques.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Vous êtes donc sûrement, comme les citoyens\\rque nous sommes, sensibles
à l'emprise de ces technologies sur nos vies,\\ret soucieux du respect
des libertés numériques qui reflètent des\\rvaleurs que nous devons
préserver.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\rNous avons essayé de traduire quelques-unes de celles-ci dans les
3\\rpactes des libertés numériques, qui déclinent 3 thèmes qui nous
sont\\rchers. Ils concernent respectivement les données libres,
l'internet\\rlibre et le logiciel libre.

.. raw:: html

   </div>

\\r

.. raw:: html

   <div>

Nous les soumettons à votre signature, comme\\rnous les avons soumis en
primeur à l'ancien président Didier Reynders\\r(nous n'avons pas encore
reçu la signature de Charles Michel) et à la\\rministreSabine Laruelle,
qui\\rles ont signés tous les 3, comme de12 candidats et 3 élus MR l'ont
fait lors du scrutin de 2010. (voir aussi la `listes des signataires de
2010 <\"http://lepacte.be/?post/2012/10/10/Les-signataires-des-pactes-principaux-en-2010\">`__).
Cette version 2012 est absolument identique à celle de 2012, moyennent
bien sûr une adaptation au type de scrutin.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Pour votre information, nous suivons dans l'outil
`**http://lepacte.be** <\"http://lepacte.be\">`__\\ret en particulier le
lien en haut à gauche «Résultats de campagne ?»,\\ren temps réel, les
signatures que nous avons reçues, totalisés aussi par\\rparti, et nous
en informons régulièrement tant la presse que nos\\rconcitoyens pour
qu'ils puissent, le 14 octobre prochain, connaître vos\\rpositions sur
ces sujets et faire un choix éclairé. Sachez que plusieurs\\rcentaines
de personnes nous ont fait savoir que cela avait eu une\\rinfluence sur
leurs votes.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Vous trouverez le **détails des textes des 3 pactes et la procédure de
signature** à
l'adresse\ **`http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__**\ \\roù
vous pourrez les signer avec votre carte d'identité électronique\\rcomme
vous l'utilisez pour remplir votre déclaration d'impôt, ou, si\\rvous ne
pouvez signer électroniquement, à remplir ce même formulaire
en\\rligne,\\rl'imprimer (version pdf à droite), le signer
manuscritement et nous le\\rtransmettre scanné ou photographié par
**email à `jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**.

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous\\rsommes à votre disposition pour en discuter si vous le souhaitez.
Par\\rexemple par téléphone au coordinateur Nicolas Pettiaux `0496 24 55
01 <\"tel:0496%2024%2055%2001\">`__ ou par email à `info@lepacte.be
 <\"mailto:info@lepacte.be\">`__
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

Nous espérons pouvoir vous\\rrencontrer un de ces jours pour discuter de
ces questions qui nous\\rtiennent à cœur et vous souhaitons, d'ici là,
une bonne dernière semaine\\rde campagne et les meilleurs résultats
possibles, et vous invitons à\\rtransmettre ce message à vos colistiers.
\\r

.. raw:: html

   </div>

.. raw:: html

   <div>

\\r
Dans l'espoir de vivre avec vous dans un société numérique plus juste,
plus respecteuse, plus solidaire et plus libre,

.. raw:: html

   </div>

.. raw:: html

   <div>

Bien à vous,

.. raw:: html

   </div>

.. raw:: html

   <div>

<SIGNATURE>
Pour la campagne `lepacte.be <\"http://lepacte.be\">`__\ 

.. raw:: html

   </div>

.. raw:: html

   </p>

