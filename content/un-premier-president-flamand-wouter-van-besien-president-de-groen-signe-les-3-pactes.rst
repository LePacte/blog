Un premier président flamand, Wouter Van Besien, président de Groen!, signe les 3 pactes.
#########################################################################################
:date: 2010-06-06 17:42
:category: Legislatives 2010
:slug: un-premier-president-flamand-wouter-van-besien-president-de-groen-signe-les-3-pactes
:status: published

Après les présidents des 4 grands partis francophones, un premier
président d'un parti flamand, Wouter Van Besien, président de Groen!,
signe les 3 pactes des libertés numériques.

Lors du meeting de campagne tenu ce dimanche 6 juin au Studio 4 par
Ecolo et Groen!, Wouter Van Besien, président de Groen!, a signé les 3
pactes des libertés numériques.&nbsp;Bien sûr les versions en
néerlandais car `LePacte.be / HetPact.be <\"\">`__ une initiative
transpartisane et bilingue.

\\r

Monsieur Van Besien rejoint donc Elio di Rupo, président du PS, Joëlle
Milquet, présidente du cdH Didier Reynders, président du MR, Sarah
Turine et Jean-Michel Javaux, co-président d'Ecolo, pour montrer aux
citoyens qu'ils pensent que les valeurs et idées de ces pactes méritent
un soutien et de l'attention, et comme le titrait Trends-Tendance ce
vendredi \ `*Le PS, le MR, le cdH et Ecolo sont au moins d’accord sur un
point : il faut défendre les «libertés numériques». Leurs présidents ont
tous signé un triple pacte en ce sens, plaidant du même coup «pour une
société numérique belge plus
ouverte»* <\"http://trends.rnews.be/fr/economie/actualite/high-tech/les-4-grands-partis-francophones-signent-un-triple-pacte-internet/article-1194747967008.htm\">`__\ * *

\\r

Nous espérons que monsieur Van Besien sera le premier d'une longue série
de présidents et candidats flamands signataires des «3 digitale pacten
».

.. raw:: html

   </p>

