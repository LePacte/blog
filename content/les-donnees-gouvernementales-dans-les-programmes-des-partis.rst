Les données gouvernementales dans les programmes des partis
###########################################################
:date: 2010-05-31 21:36
:category: Free Data
:slug: les-donnees-gouvernementales-dans-les-programmes-des-partis
:status: published

Ce 31 mai 2010, les programmes 2010 des 4 principaux
partis\\rfrancophones sont disponibles, nous les avons donc parcouru
pour vous.

Pour le CdH:
~~~~~~~~~~~~

\\r

Dans la `5ème
partie <\"http://www.lecdh.be/sites/default/files/partie5-2pages_0.pdf\">`__:

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

| Garantir l’information\\rdu citoyen
| L’information du citoyen est à la fois un droit fondamental,\\rune
garantie d’efficacité et une garantie de l’inclusion de tous dans\\rle
processus politique. Elle constitue un élément essentiel de la\\rréforme
de la gouvernance. Elle permet ainsi d’améliorer la confiance\\rque les
citoyens peuvent avoir envers leurs institutions.
| a.\\rUtiliser les méthodes multimédia pour fournir l’information
| Le cdH\\rpropose de :

.. raw:: html

   </li>

\\r

-   Etablir une politique de diffusion systématique des\\rdocuments sur
   les sites Internet fédéraux, notamment de tous les\\rdocuments
   préparatoires à une décision ainsi que les rapports
   extérieurs\\rréalisés au profit des autorités, dans un souci de
   transparence.

\\r

.. raw:: html

   </ul>

\\r

Pour Ecolo:
~~~~~~~~~~~

\\r

Programme `Démocratie - Ethique
politique <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Democratie_Ethique_politique.pdf\">`__:

\\r

-  Assurer au citoyen\\rl’accès à l’information
   Le développement de la participation citoyenne\\rrepose sur un
   pré-requis, à savoir que chacun et chacune ait un libre\\raccès aux
   décisions publiques. En d’autres termes, pour être en mesure\\rde
   participer à la décision publique, il faut pouvoir disposer
   de\\rl’information. Le droit à pouvoir consulter les actes
   administratifs et,\\ren particulier, les actes préparatoires à la
   décision finale de\\rl’autorité publique a beau être consacré dans
   notre Constitution\\r(article 32) ou dans des instruments de droit
   international ou européen\\r(Convention d’Aarhus), il n’en demeure
   pas moins sujet à difficultés et\\rtracasseries sur le terrain et ce,
   aux différents niveaux de pouvoir.
   Par\\rconséquent, Ecolo propose de systématiser une publicité active
   des\\ractes administratifs, en ce compris celle des avis et
   actes\\rpréparatoires aux décisions publiques (avis du Conseil d’Etat
   et\\rd’autres instances de consultation) et de développer les
   supports de la\\rcommunication à cet égard.

\\r

Pour le MR :
~~~~~~~~~~~~

\\r

`Programme\\r2010 <\"http://www.mr.be/media/images/content/Programme2010.pdf\">`__:

\\r

-  quelques notions de transparence de l'administration mais rien\\rde
   très clair (cf Fonction Publique)

\\r

Pour le PS :
~~~~~~~~~~~~

\\r

`Programme\\r2010 <\"http://www.ps.be/_iusr/programme_2010___version_finale.pdf\">`__:

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

| Réduire la fracture numérique:
| Dans l’esprit du Plan National de lutte\\rcontre la fracture
numérique, porté par nos ministres sous les\\rprécédentes législatures,
le PS lutte contre l’émergence d’une société\\rde l’information à deux
vitesses en levant les obstacles financiers,\\rculturels, sociologiques
et éducatifs qui empêchent un accès libre et\\régal de tous les citoyens
aux nouvelles technologies. Concrètement, pour\\ratteindre ces
objectifs, le PS propose de : [...]

.. raw:: html

   </li>

\\r

-  favoriser\\rl’émergence d’un domaine public de qualité notamment par
   la\\rnumérisation et la mise en ligne des archives des
   institutions\\rculturelles ou scientifiques fédérales ;

\\r

.. raw:: html

   </ul>

.. raw:: html

   </p>

