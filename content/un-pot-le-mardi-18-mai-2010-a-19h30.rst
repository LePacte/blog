Un pot le mardi 18 mai 2010 à 19h30
###################################
:date: 2010-05-17 08:58
:category: Legislatives 2010
:slug: un-pot-le-mardi-18-mai-2010-a-19h30
:status: published

Nous vous proposons de nous retrouver pour le premier pot de la campagne
2010, histoire de mettre des visages et des voix\\rsur des noms et
adresses emai, ou de se revoir pour ceux qui étaient de la partie
l'année passée.

Date: **mardi 18 mai 2010 à 19h30**

\\r

Hasard des calendriers surchargés, la prochaine fois on vous laissera
plus de marge, promis!

\\r

Lieu : « **Etc...** », petit bistrot de quartier modernisé avec terasse
situé à Etterbeek – La Chasse, avenue Jules Malou, fréquenté par
quelques libristes avec wifi pour les amis.

\\r

La localisation sur la carte libre
`OpenStreetMap <\"http://www.osm.org/\">`__ est fournie
`http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17 <\"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\">`__
(au centre du quadrilatère rouge)

| \\rAccès :
| \\r

-  Tram 81 & 83 Bus 36, 59, 60, 95 (3/6 min)
-  Train par la gare Bruxelles/Luxembourg (20 min) ou bus 36 ou la gare
   d'Etterbeek (15 min) ou bus 95

| \\rBières spéciales € 2.5 Jupiler € 1.4

.. raw:: html

   </p>

