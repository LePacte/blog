Qu'est-ce que LePacte.be ?
##########################
:date: 2010-06-05 00:46
:slug: quest-ce-que-lepactebe
:status: published

Le site lepacte.be se veut un outil mis à la disposition des communautés
et individus belges défendant diverses valeurs de liberté à l'ère
numérique: logiciel libre, standards ouverts, données libres et
ouvertes, notamment gouvernementales, un Internet libre, une éthique au
vote électronique, la brevetabilité du logiciel, la vente liée, etc

Le but est de faire prendre conscience aux responsables politiques du
nombre de citoyens se sentant concernés par ces enjeux et de
l\\\\'importance qu\\\\'ils y attachent.

Le site lepacte.be a également pour objectif de permettre à chacun
d'avoir les élements de compréhension et de débat sur des problématiques
parfois complexes et techniques, puis de faire son choix en conscience.
Lepacte.be est porté un groupe qui est neutre sur le plan de la
politique mandataire et ne prend donc pas position en faveur de tel ou
tel parti politique.

Lepacte.be a été lancé le 30 mai 2010 à l'initiative de citoyens belges
bénévoles qui dédient des ressources humaines et matérielles pour porter
le projet.

Lepacte.be s'inspire librement des campagnes
`candidats.fr <\"http://candidats.fr\">`__ et
`candidats.be <\"http://candidats.be\">`__ organisées par l'April en
2009 et 2010.

.. raw:: html

   </p>

