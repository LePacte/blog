Festival des libertés : n'oublions pas les libertés numériques
##############################################################
:date: 2012-10-18 14:18
:slug: festival-des-libertes-noublions-pas-les-libertes-numeriques
:status: published

| 
| Je viens de redécouvrir le Festival des libertés qui se tient
essentiellement au Théatre national du 18 au 27/10/2012. Voir
`www.festivaldeslibertes.be <\"http://www.festivaldeslibertes.be/\">`__
| \\r
| Il me semble que l'initiative à l'air très chouette ... mais que
les\\rorganisateurs ont peu mis en évidence le fait que que aujourd'hui
beaucoup d'activités\\ront lieu en ligne et/ou de manière électroniques
et que, si nous n'y\\rprenons pas garde, les libertés chèrement acquises
et toujours à\\rdéfendre et développer dans le monde physique sont aussi
menacées dans\\rle monde électronique où la surveillance, le contrôle,
le déni de choix\\rparfois (ex du système d'exploitation - sic- quand
vous acheter un\\rordinateur ou un smartphone) sont presque la règle.

\\r

Bon, il y a quand même 2 sujets qui serviront de base à des débats :

\\r

-  `Internet, un espace récupéré ou à se réapproprier
   ? <\"http://www.festivaldeslibertes.be/fase6.php?event=599#599\">`__
   le samedi 27/10/2012 à 17h00,
-  `L’art dans la toile : piège ou
   tremplin <\"http://www.festivaldeslibertes.be/fase6.php?event=601#601\">`__,
   samedi 27/10/2012 à 20h30,

\\r

Qui irait avec moi faire un tour ce WE par exemple (si
les\\rorganisateurs acceptent) et montrer des logicielsl libres, du
matériel libre (arduino et raspberry-pi) et quelques jolies\\raffiches
imprimées et parfois plastifiées à l'occasion de la conférence\\rde R
Stallman ?

\\r

Quelques portables et projecteurs vidéos, en plus des\\raffiches
suffiraient.

.. raw:: html

   </p>

