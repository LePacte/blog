Dans les programmes des partis ou les réponses : Écolo, PS et MR
################################################################
:date: 2009-06-05 12:28
:category: Européennes et régionales 2009
:slug: dans-les-programmes-des-partis-ou-les-reponses-ecolo-ps-et-mr
:status: published

En lisant attentivement les programmes des partis politiques ou les
réponses de quelques ténors, parfois avec l'aide de ceux-ci, nous avons
glané des points liés au logiciel libre. Nous nous en réjouissions et
vous les présentons bruts, pour que vous disposiez d'une information
complémentaire et puissiez nourrir votre réflexion.

Ecolo : les écologistes!
------------------------

Dans son programme publié en ligne à l'adresse
http://web4.ecolo.be/spip.php?rubrique219, Ecolo affirme ses
revendications de

Programme Ecolo 2009 - Livre V « Pour une société démocratique » - Chapitre 1 « Démocratie » - p 12/65!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir sur
`http://web4.ecolo.be/spip.php'article1383 <\"http://web4.ecolo.be/spip.php'article1383\">`__

« de systématiser l'utilisation des logiciels libres de droits dans le
champ public (pouvoirs publics, administrations, écoles...). »

Programme Ecolo 2009 - Livre V « Pour une société démocratique » - Chapitre 1 « Démocratie » - p 14/65
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir sur
`http://web4.ecolo.be/spip.php'article1383 <\"http://web4.ecolo.be/spip.php'article1383\">`__

« de soutenir un développement économique ouvert en refusant que les
logiciels soient considérés comme des inventions relevant de la
législation sur les brevets ; pour Ecolo, les logiciels sont des
langages qui doivent rester accessibles à tous ; dans ce cadre, il
convient par ailleurs de s'assurer du respect de l'interdiction de la
vente liée (qui conditionnerait par exemple l'achat d'un ordinateur à
celui d'un logiciel), afin de permettre à chacun de choisir voire de
développer ses propres outils logiciels ; »

Programme Ecolo 2009 - Livre V « Pour une société démocratique » - Chapitre 3 « Service public » - p 41/65
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir sur
`http://web4.ecolo.be/spip.php'article1393 <\"http://web4.ecolo.be/spip.php'article1393\">`__

« Ecolo entend également voir systématiser l'acquisition de logiciels
informatiques utilisant des standards ouverts, dans le but de garantir
l'interopérabilité des systèmes, de sécuriser l'archivage des contenus
numériques et garantir la pérennité de l'accès électronique aux contenus
numériques. »

Programme Ecolo 2009 - Livre V « Pour une société démocratique » - Chapitre 3 « Service public » - p 37/65
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir sur
`http://web4.ecolo.be/spip.php'article1391 <\"http://web4.ecolo.be/spip.php'article1391\">`__

::

« Dans ce cadre, Ecolo soutient fermement que les logiciels développés
spécifiquement pour - et de manière plus générale - utilisés par des
autorités publiques, doivent être autant que possible des logiciels
libres afin de garantir notamment la transparence et la pérennité des
investissements publics dans ces logiciels. Cela permettra aussi plus
facilement à d'autres autorités de réutiliser ces logiciels pour leur
propre administration électronique. Il en va de même pour les
\\"tiers\\" qui doivent communiquer sous format électronique avec une
autorité publique. »

Programme Ecolo 2009 - Livre IV « Pour une société qui se développe » - Chapitre 5 « Recherche et Innovation » - p 76/77
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir sur
`http://web4.ecolo.be/spip.php'article1433 <\"http://web4.ecolo.be/spip.php'article1433\">`__

::

« Sur base du modèle du logiciel libre, de nombreux scientifiques, et
plus particulièrement dans les domaines de la physique et de la
médecine, ont lancé des revues scientifiques en libre accès. La
Déclaration de Berlin sur le Libre Accès à la Connaissance en Sciences
exactes, Sciences de la vie, Sciences humaines et sociales est
explicite : « De nouvelles possibilités de diffusion de la connaissance,
non seulement sous des formes classiques, mais aussi, et de plus en
plus, en s'appuyant sur le paradigme du libre accès via l'Internet,
doivent être soutenues. Nous définissons le libre accès comme une source
universelle de la connaissance humaine et du patrimoine culturel ayant
recueilli l'approbation de la communauté scientifique. »

PS : le parti socialiste
------------------------

Les programmes sont sur le site du PS à l'adresse
`http://www.ps.be/elections2009/programmes/ <\"http://www.ps.be/elections2009/programmes/\">`__

Pour la Région wallonne (p.108)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir aussi
`http://www.ps.be/elections2009/programmes/programmerw/ <\"http://www.ps.be/elections2009/programmes/programmerw/\">`__

Les logiciels libres sont des outils indispensables pour assurer
l’accessibilité de tous aux technologies de l’information tout en
garantissant la liberté de chaque utilisateur. Ils ont ainsi une
influence sur la position compétitive de nos entreprises,
particulièrement celle des petites et moyennes entreprises, et sur
l’innovation technologique. Le PS a affirmé à plusieurs reprises son
attachement aux valeurs et au modèle alternatif porté par le logiciel
libre.

Au niveau wallon, le PS s'engage à :

-  promouvoir l’usage des logiciels libres dans les Administrations, les
   cabinets ministériels et dans les communes ;

-  promouvoir la création d’une plate-forme d’échange et de coopération
   en matière informatique entre tous les acteurs publics actifs dans le
   domaine des technologies de l’information ;

-  promouvoir l'utilisation des logiciels libres auprès des citoyens et
   des entreprises, notamment par des actions de sensibilisation et de
   formation ;

-  promouvoir et former à l’utilisation des logiciels libres au sein des
   écoles ;

-  plaider en faveur de la plus grande interopérabilité des systèmes.

Pour la Communauté française (p.92)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir aussi
`http://www.ps.be/elections2009/programmes/programmecf/ <\"http://www.ps.be/elections2009/programmes/programmecf/\">`__

Les logiciels libres sont des outils indispensables pour assurer
l’accessibilité de tous aux technologies de l’information tout en
garantissant la liberté de chaque utilisateur. Le mouvement du logiciel
libre est un phénomène mondial de travail collaboratif à travers lequel,
des communautés d'utilisateurs et de développeurs de logiciels
s'entraident pour produire des logiciels de qualité. Ils contribuent à
réduire la fracture numérique dans de nombreux pays en particulier dans
le Sud. Le PS a affirmé à plusieurs reprises son attachement aux valeurs
et au modèle alternatif porté par le logiciel libre. En ce sens, il
propose :

-  de développer les formations aux logiciels libres dont la portée
   pédagogique est confirmée ;

-  de promouvoir l’usage des logiciels libres dans les administrations
   et les cabinets ministériels ;

-  de promouvoir l’utilisation des logiciels libres auprès des citoyens
   et des entreprises, notamment par des actions de sensibilisation
   et/ou de formation;

-  de promouvoir l’utilisation des logiciels libres au sein des écoles ;

-  de plaider en faveur de la plus grande interopérabilité des systèmes

Pour la Région de Bruxelles capitale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voir aussi
`http://www.ps.be/elections2009/programmes/programmebxl/ <\"http://www.ps.be/elections2009/programmes/programmebxl/\">`__

(p.100) Pour un service communal dynamique, moderne et efficace, le PS
veut encourager les communes à passer aux logiciels libres et à acheter
prioritairement des produits éthiques.

(p.108) : Passer aux logiciels libres dans l'administration et les
pararégionaux bruxellois

Depuis longtemps le PS bruxellois bataille pour l'utilisation
systématique des logiciels libres dans l'administration régionale. En
effet, les logiciels libres sont libres de droits d'utilisation et de
reproduction. Ils sont donc véritablement accessibles à toutes et tous.
Ils garantissent aussi une certaine protection de la vie privée puisque
leur code source est connu (le code source est écrit dans un langage
informatique usuel). En plus, cela permettrait de doter l'administration
d'une réelle indépendance vis-à-vis des multinationales de logiciels
propriétaires.

Le PS bruxellois propose de :

-  adopter une ordonnance imposant l'utilisation dans les
   administrations régionales de formats ouverts et de logiciels libres
   afin de garantir l'accessibilité des informations publiques et la
   confidentialité des données personnelles ;

-  veiller à encourager l’utilisation de formats ouverts et de logiciels
   libres auprès des Bruxellois et des communes, notamment par des
   actions de sensibilisation et de formation ;

Réponse de Louis Michel, un ténor du MR
---------------------------------------

Monsieur l’Administrateur,

Votre courrier du 1er juin relatif aux logiciels libres m’est bien
parvenu, et je vous en remercie

Cette question mérite une approche nuancée. En effet, je reconnais que
le logiciel libre est intéressant à plus d'un titre, y compris pour des
administrations. En premier lieu, il évite de se faire \\"emprisonner\\"
en étant obligé de recourir systématiquement au même fournisseur, pour
des applications qui sont entièrement \\"propriétaires\\", ne sont
compatibles avec elles-mêmes et dont le code n'est connu que de
l'éditeur.

Il faut toutefois garder à l'esprit qu'une administration doit aussi et
avant tout répondre au principe de continuité du service...d'où
l'importance de trouver des solutions logicielles qui répondent à la
fois à des standards ouverts, mais aussi d'avoir un certain nombre
d'assurances sur le \\"service après-vente\\" des logiciels. Les
administrations disposent aujourd'hui de cahiers des charges qui leur
permettent,

Au niveau fédéral, le MR a toujours exigé que tous les logiciels
répondent aux standards \\"libres\\" d'échanges de fichier; par ailleurs
les administrations maîtrisent souvent très bien les cahiers des charges
qu'elles édictent lorsqu'elles ont besoin d'un nouveau logiciel. La
plupart du temps, une solution \\"libre\\" existe et est prise en
compte.

Restant à votre écoute, je vous prie de recevoir, Monsieur
l’Administrateur délégué, l’expression de mes sentiments distingués.

Louis MICHEL

.. raw:: html

   </p>

