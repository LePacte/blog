De nouveaux tableaux et graphiques : les têtes de listes s'affichent
####################################################################
:date: 2010-06-11 06:15
:category: Legislatives 2010
:slug: de-nouveaux-tableaux-et-graphiques-les-tetes-de-listes-saffichent
:status: published

On ne chôme pas dans les partis, ni sur `lepacte.be <\"\">`__ . Il est
maintenant simple de savoir aussi combien de têtes de liste (effectif ou
suppléant) de quels partis ont signé les pactes.

Notre outil de suivi de campagne étant un logiciel libre, et l'équipe
possédant les compétences pour l'adapter, elle le modifie au fur et à
mesure des besoins. Un travail très utile car il met mieux en lumière
les actions des partis et les engagements des candidats notamment.

\\r

Le dernier travail, `visible sur la page principale de
l'outil <\"legis2010\">`__, permet de savoir, après les nombres de
signataires de chaque parti, le nombre de têtes de listes (ceux dont qui
ont le plus de chance de siéger et de devenir des décideurs à défaut
d'être les chefs de groupe et donc les donneurs de tendances).

.. raw:: html

   </p>

