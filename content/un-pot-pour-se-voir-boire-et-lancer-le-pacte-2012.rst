Un pot pour se voir, boire et lancer le pacte 2012
##################################################
:date: 2012-06-30 09:51
:category: Communales 2012
:slug: un-pot-pour-se-voir-boire-et-lancer-le-pacte-2012
:status: published

Date: **la semaine du 16 au 19 juillet 2012 de 20h à 22h **

\\r

Lieu : « **Etc...** », petit bistrot de quartier\\rmodernisé avec
terasse situé à Etterbeek – La Chasse, avenue Jules\\rMalou, fréquenté
par quelques libristes avec wifi pour les amis.C'est notre lieu
maintenant habituel.

\\r

Merci de compléter
le \ `framadate <\"http://framadate.org/9hx7auyjymbx4lb8\">`__ pour
indiquer la date qui vous convient le mieux, votre présence possible ou
vraisemblable, ainsi que celle de vos amis.

\\r

La localisation sur la carte libre
`OpenStreetMap <\"http://www.osm.org/\">`__ est fournie
`http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17 <\"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\">`__
(au centre du quadrilatère rouge)

| \\rAccès :
| \\r

-  Tram 81 & 83 Bus 36, 59, 60, 95 (3/6 min)
-  Train par la gare Bruxelles/Luxembourg (20 min) ou bus 36 ou la gare
   d'Etterbeek (15 min) ou bus 95

| \\rBières spéciales € 2.5 Jupiler € 1.4
| Au plaisir de vous y voir

.. raw:: html

   </p>

