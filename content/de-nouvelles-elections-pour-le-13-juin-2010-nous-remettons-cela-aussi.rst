De nouvelles élections pour le 13 juin 2010 ... nous remettons cela aussi
#########################################################################
:date: 2010-05-16 09:33
:category: Legislatives 2010
:slug: de-nouvelles-elections-pour-le-13-juin-2010-nous-remettons-cela-aussi
:status: published

Plus de gouvernement fédéral. Des élections le 13 juin 2010. Donc du
travail pour nous dans le cadrede candidats.be afin de motiver les
candidats à soutenir, défendre, promouvoir le logiciel libre et les
contenus libres.

Après les succès de l'année passée, nous n'imaginions pas devoir nous
remettre au travail aussi tôt. Sans doute les hommes et femmes
politiques n'imaginaient pas plus devoir repasser par les urnes ... et
la sanction des électeurs. Donc de devoir montrer ce qu'ils font et ont
fait, et comment ils ont tenus leurs promesses.

Cette année, nous innovons un peu. En effet, les contenus libres sont le
direct prolongement des outils, les logiciels libres. Ils doivent donc
autant être soutenus, promus, et utilisés. Surtout par les institutions
publiques comme les administrations et le monde de l'éducation. De tels
contenus sont par exemple, les données géographiques collectées par
l'état et les administrations (cartographie, données cadastrales ...) ou
les contenus pédagogiques que sont les supports de cours, exercices ...

Le premier billet d'une nouvelle série pour 2010 est une information aux
candidats aux élections : ils seront sollicités, et aussi un appel au
soutien de tous ceux pour lesquels ces thèmes sont importants.

Vous pouvez suivre l'évolution de notre travail sur l'outil de gestion
de campagne http://candidats.be/legis2010/

Si vous voulez participer et donner un peu d'aide et de temps (même un
peu est bienvenu), rejoignez-nous sur la `liste de distribution de
courriers
électroniques <\"http://www.april.org/wws/info/candidatsbe\">`__ et le
`wiki <\"http://wiki.april.org/w/CandidatsBe\">`__

.. raw:: html

   </p>

