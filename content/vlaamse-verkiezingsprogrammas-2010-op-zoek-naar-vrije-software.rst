Vlaamse verkiezingsprogramma's 2010: op zoek naar Vrije Software
################################################################
:date: 2010-06-06 06:23
:category: Free Software
:slug: vlaamse-verkiezingsprogrammas-2010-op-zoek-naar-vrije-software
:status: published

Zoals voor de programma's van de franstalige partijen al `is
gebeurd <\"http://www.candidats.be/index.php?post/2010/05/31/Dans-les-programmes-des-partis-II\">`__,
gaan we op zoek naar wat onze politici te zeggen hebben over Vrije
Software. De oogst is redelijk mager, maar er zijn talrijke
aanknopingspunten.

Zoals voor de programma's van de franstalige partijen al `is
gebeurd <\"http://www.candidats.be/index.php?post/2010/05/31/Dans-les-programmes-des-partis-II\">`__,
gaan we op zoek naar wat onze politici te zeggen hebben over Vrije
Software. De oogst is redelijk mager, maar er zijn talrijke
aanknopingspunten.

\\r

`Programma Groen! <\"http://groen.be/uploads/programma/Groen_verkiezingsprogramma_2010.pdf\">`__
================================================================================================

\\r

-  64: (Armoede:) We werken drempels in het onderwijs weg.
   `... <\"...\">`__ Ook wil Groen! meer middelen in lokale projecten om
   de digitale kloof te dichten, zodat ook mensen in armoede en hun
   kinderen toegang hebben tot informatie via internet.
   `... <\"...\">`__

\\r

Vrije software past perfect in dit plaatje, zoals ook blijkt uit
bestaande initiatieven in bibliotheken en in het onderwijs.

\\r

-  30: `... <\"...\">`__ Groene ICT `... <\"...\">`__
-  174: `... <\"...\">`__ Informatisering justitie `... <\"...\">`__

\\r

Ook hierin kan Vrije Software een belangrijke rol spelen, de gebruikers
ervan zijn namelijk niet aan de auteurs van de software gebonden en
kunnen veel flexibeler optreden.

\\r

-  5: Zoveel mogelijk diensten worden gedigitaliseerd en online
   beschikbaar (bijv. milieuvergunningen). Er komen centrale
   gegevensdatabanken `... <\"...\">`__
-  17: We versterken het Globaal Medisch dossier. We willen dat de
   relevante informatie uit het globaal medisch dossier in de toekomst
   beschikbaar is voor andere zorgverstrekkers die de patiënt
   behandelen. Bovendien willen we werk maken van het elektronisch
   medisch dossier, dat het registreren en uitwisselen van gegevens
   sterk zal bevorderen.

\\r

Open standaardformaten zijn het meest geschikt om deze doelen te
bereiken. De federale overheid kiest reeds voor OpenDocument-formaten
voor interne en externe communicatie
`http://en.wikipedia.org/wiki/OpenDo... <\"http://en.wikipedia.org/wiki/OpenDocument_adoption#Belgium\">`__.

\\r

`Programma sp.a <\"http://www.s-p-a.be/media/uploads/programs/Volledig_kiesprogramma.pdf\">`__
==============================================================================================

\\r

-  82: We schenken bijzondere aandacht aan de bescherming van de privacy
   van de consumenten in het kader van nieuwe technologieën en sociale
   netwerken.

\\r

Een gebruiker kan maar over privacy op haar/zijn computer beschikken als
zij/hij de gebruikte software volledig kan onderzoeken en aanpassen, of
dit kan laten doen: kortom door Vrije Software te gebruiken.

\\r

`Programma CD&V <\"http://verkiezingen.cdenv.be/sites/verkiezingen.cdenv.be/files/2010_Federaal%20Verkiezingsprogramma%20CD&V_goedgekeurd%20congres22052010.pdf\">`__
=====================================================================================================================================================================

\\r

-  Meer te investeren in het dichten van de digitale kloof. Een
   essentiële voorwaarde om uit te groeien tot een dynamische
   kennisregio.
-  (Belgacom:) `... <\"...\">`__ En laagdrempelige telecom-oplossingen
   te laten aanbieden die de digitale kloof meedichten.

\\r

Hier worden de economische implicaties van de digitale kloof benadrukt.
Ook vanuit dit opzicht heeft Vrije Software een belangrijke rol te
spelen.

\\r

-  `... <\"...\">`__ De algemene invoering van een elektronisch en
   gedeeld medisch

\\r

patiëntendossier is hierbij een belangrijk instrument. `... <\"...\">`__

\\r

-  Te werken aan een goed uitgebouwd E-health platform voor het beheer
   en de uitwisseling van informatie over de gezondheid van de patiënt,
   mits de privacy en het medisch geheim voldoende worden gerespecteerd.
   `... <\"...\">`__

\\r

Bovenop de punten van Groen! wordt hier de aandacht gevestigd op de
privacy, nog een reden om hiervoor Vrije Software te gebruiken.

\\r

-  `... <\"...\">`__ Op het niveau van het Hof van Beroep moet een
   marktregulator gecreëerd worden voor problematieken inzake
   `... <\"...\">`__ ICT, mededinging. `... <\"...\">`__

\\r

Niet alleen voor Vrije Software en Open Standaarden relevant.

\\r

-  Informatisering justitie/politiediensten...

\\r

Cfr. supra.

\\r

-  Om auteursrechten, naburige rechten en leenvergoedingen beter en
   duidelijker te regelen. Gebruikers vinden hun weg niet meer in de
   veelheid aan regelgeving. `... <\"...\">`__

\\r

Goede afspraken in verband met deze zaken zijn zeer belangrijk voor
Vrije Software, maar het programma gaat niet in detail.

\\r

`Programma Open VLD <\"http://pub.openvld.be/Bestanden/prog.pdf\">`__
=====================================================================

\\r

-  Een vernieuwende en duurzame economie: `... <\"...\">`__ Ten slotte
   bestaat er ook veel creatief ondernemerschap in wat men
   “social-profit activiteiten” kan noemen. Voorbeelden: Wikipedia, open
   source software, `... <\"...\">`__

\\r

In de marge vinden we hier ongeveer een vermelding van Vrije Software,
de term 'Open Source' ontwijkt namelijk de relevante ethische aspecten.
Het programma behandelt hier geen duidelijk standpunt of voorstel.

\\r

-  `... <\"...\">`__ Ook zal nog meer de kaart moeten getrokken worden
   van informatisering (E-Health). `... <\"...\">`__

\\r

Cfr. supra.

\\r

-  `... <\"...\">`__ Het ‘Nieuw Strategisch Concept voor de NAVO’ moet
   antwoorden bieden op de uitdagingen van het komende decennium zoals:
   `... <\"...\">`__ en cyber aanvallen. `... <\"...\">`__

\\r

Wanneer het niet toegelaten is de werking van bijvoorbeeld een
besturingssysteem volledig te doorgronden kan er geen garantie zijn
tegen computerinbraak, via fouten in de software of via achterpoortjes.
Een onafhankelijke ICT-infrastructuur is slechts mogelijk met Vrije
Software.

\\r

`Programma N-VA <\"http://www.n-va.be/files/default/nva_images/documenten/verkiezingsprogramma%20N-VA%202010.pdf\">`__
======================================================================================================================

\\r

-  5.4 Een ondernemingsvriendelijke overheid

\\r

`... <\"...\">`__ Vermindering van regeldruk voor ondernemers en een
betere uitvoering gaat hand in hand met de invoering van standaardisatie
van gegevens en processen met behulp van open standaarden. Op basis van
een gegevensstandaard wordt een eenduidige vertaling van de wetgeving
gemaakt in een gegevenswoordenboek dat ingebouwd wordt in de\\rsystemen
van zowel de overheid als het bedrijfsleven.

\\r

De enige expliciete vermelding van Open Standaarden in een Vlaams
verkiezingsprogramma! Door transparantie in de structuur van de gegevens
verhindert men een monopoliepositie van één (of enkele) bedrijven.

\\r

-  4.6 Onze gezondheidszorg

\\r

`... <\"...\">`__ Verder pleiten we voor een verregaande digitalisering
met respect voor de privacy `... <\"...\">`__

\\r

Cfr. CD&V

\\r

-  5.3 Durven afslanken

\\r

`... <\"...\">`__ in te zetten op een doorgedreven gebruik van ICT in de
administratie `... <\"...\">`__

\\r

Cfr. supra.

\\r

`Programma LDD <\"http://www.uwcenten.be\">`__
==============================================

\\r

In de sloganeske brochures wordt niets vermeld dat relevant is voor
Vrije Software.

\\r

`Programma VB <\"http://www.vlaamsbelang.org/2010_verkiezingsprogramma/\">`__
=============================================================================

\\r

Deze webpagina bestaat uit een Flash bestand dat niet kan bekeken worden
met Gnash, de vrije Flash speler. Het programma is dan ook letterlijk
niet aangepast aan Vrije Software of Open Standaarden.

\\r

Conclusie
=========

\\r

In tegenstelling tot hun collega's bezuiden de taalgrens zijn de Vlaamse
politici (of hun partijideologen) dus blijkbaar nauwelijks op de hoogte
van Vrije Software, waarvoor we hun steun willen vragen. Toch hebben
vorig jaar bij de Europese en Vlaamse verkiezingen een aantal onder hen
het Vrije Software Pact ondertekend. Bij nader contact zal moeten
blijken in hoeverre zij geïnteresseerd zijn om zich achter deze
onderwerpen te scharen.

\\r

Vragen, opmerkingen en aanvullingen zijn welkom op het adres
`info@hetpact.be <\"mailto:info_@_hetpact.be\">`__

.. raw:: html

   </p>

