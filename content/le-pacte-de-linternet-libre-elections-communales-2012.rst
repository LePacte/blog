Le Pacte de l'Internet Libre - Élections communales 2012
########################################################
:date: 2012-10-06 22:21
:slug: le-pacte-de-linternet-libre-elections-communales-2012
:status: published

Ce document est également téléchargeable en version pdf ici: `Pacte de
l'Internet Libre 2012
(pdf) <\"public/Le_pacte_d_internet_libre_2012.pdf\">`__.

\\r

Vous pouvez donc soit `signer le pacte de manière électronique et sûre
sur notre site <\"https://jesigne.lepacte.be/\">`__, soit remplir le
formulaire ici, imprimer cette page web, la signer et nous la renvoyer
par courrier, fax ou (scanné) par email, ou alors remplir le formulaire
du pdf, imprimer le pdf, le signer et nous le renvoyer.

\\r

**LE\\rPACTE DE l'INTERNET LIBRE**

\\r

**Un\\rbien commun à protéger et à développer**

\\r

**Je\\rsuis candidat(e) aux élections communales 2012**

\\r

**J'ai\\rconscience que**

\\r

\\rL'accès à Internet est devenu nécessaire pour l'exercice de droits
fondamentaux tels que la liberté d'expression.

\\r

**Je m'engage donc à**

\\r

\\r

.. raw:: html

   <form name="\&quot;Form\&quot;">

\\r

.. raw:: html

   <div align="\&quot;JUSTIFY\&quot;">

\\r
\\rNom :\\r\\rPrénom : \\rParti :

\\r
Adresse :\\r

\\r
Code\\rpostal : \\rVille :

\\r
| Courriel :\\r\\rTel :
| Fait\\rle : \\rÀ :

\\r

.. raw:: html

   </div>

\\r

.. raw:: html

   </form>

\\r

\\rSignature :

\\r

\\rÀ renvoyer par\\rcourrier, fax ou (scanné) par email à :

\\r

\\r\ **Nicolas\\rPettiaux**

\\r

\\r\ **Avenue du\\rPérou 29 - 1000 Bruxelles**

\\r

\\r\ **Fax :\\r02 350 12 66 - email :
`jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**

\\r

\\r\ **Contact :\\r\ `info@lepacte.be <\"mailto:info@lepacte.be\">`__ ou
gsm 0496 24\\r55 01**

| \\r

***Document\\rde l'initiative `LePacte.be <\"../\">`__ a en
collaboration avec\\r\ `Nurpa <\"http://nurpa.be/\">`__\\ret
`Hadopi\\rMayonnaise <\"http://hadopimayonnaise.be/\">`__***

.. raw:: html

   </p>

