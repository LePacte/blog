Le Logiciel Libre dans les programmes des partis (II)
#####################################################
:date: 2010-05-30 23:06
:category: Free Software
:slug: le-logiciel-libre-dans-les-programmes-des-partis-ii
:status: published

Ce 30 mai 2010, les programmes 2010 des 4 principaux
partis\\rfrancophones sont disponibles, nous les avons donc parcouru
pour vous.

Pour le CdH :
~~~~~~~~~~~~~

\\r

Cette année, on retrouve pour la première fois les termes \\"logiciel
libre\\" dans un programme du CdH.

\\r

Ainsi dans la `première
partie <\"http://www.lecdh.be/sites/default/files/partie1-2pages_5.pdf\">`__:

\\r

-  Garantir des droits au consommateur dans l’univers numérique
   Le CdH propose de: [...]

   -  Favoriser l'utilisation des logiciels libres que ce soit au niveau
      des configurations préinstallées ou au niveau de l'utilisation par
      les pouvoirs publics ;

   \\r

\\r

Pour Ecolo :
~~~~~~~~~~~~

| \\rProgramme `Service Public -
Gouvernance <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Service_public_Gouvernance.pdf\">`__:

-  Ecolo considère que les services publics doivent répondre à des
   exigences particulières de responsabilité sociale et soutient à ce
   titre : [...]

   -  l’utilisation de logiciels informatiques utilisant des standards
      ouverts, dans le but de garantir l’interopérabilité des systèmes,
      de sécuriser l’archivage des contenus numériques et de garantir la
      pérennité de l’accès électronique aux contenus numériques.

   \\r

| \\rProgramme `Société de
l'Information <\"http://web4.ecolo.be/IMG/pdf/Axe_Democratie_et_Gouvernance_-_Societe_de_l_information.pdf\">`__:

-  Afin que les TIC ne se transforment pas en un nouveau fossé social,
   il importe de favoriser l’accès et la participation de chacun à ces
   nouveaux médias. A cette fin, Ecolo propose notamment : [...]

   -  de maintenir l'open source, un langage technique et un trafic des
      données ouverts : quiconque doit pouvoir participer à la
      construction d’une société de l’information, sans devoir payer des
      royalties ou réinventer un langage pour communiquer avec d’autres
      systèmes d’information ; des protocoles et standards ouverts,
      labellisés par une norme publique, permettront par ailleurs
      d'intégrer plus facilement des modes d'accès adaptés aux personnes
      handicapées ; enfin, Ecolo défend le principe de neutralité du net
      ;
   -  de systématiser l’utilisation des logiciels libres de droits dans
      le champ public (pouvoirs publics, administrations, écoles …).

   \\r

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

L’exploitation des TIC représente aussi une source considérable de
profit. [...] Ecolo entend soutenir la mise en place d’un cadre de
régulation adapté permettant d’orienter le développement de ce marché en
vue d’un accès garanti à tous et d’une société de l’information durable.
Pour ce faire, Ecolo propose : [...]

.. raw:: html

   </li>

\\r

-  de permettre à chacun de choisir voire de développer ses propres
   outils logiciels ; Ecolo souhaite ainsi lutter contre la présence
   automatique de logiciels imposés par les fabricants de matériel
   informatique, en appliquant de façon plus stricte les dispositions
   visant à lutter contre les pratiques commerciales déloyales telles
   que définies dans la nouvelle loi sur les pratiques de marché ;

\\r

.. raw:: html

   </ul>

\\r

Pour le MR :
~~~~~~~~~~~~

\\r

`Programme
2010 <\"http://www.mr.be/media/images/content/Programme2010.pdf\">`__:

\\r

-  aucune mention des logiciels libres ou des standards ouverts

\\r

Pour le PS :
~~~~~~~~~~~~

\\r

`Programme\\r2010 <\"http://www.ps.be/_iusr/programme_2010___version_finale.pdf\">`__:

\\r

.. raw:: html

   <ul>

.. raw:: html

   <li>

Réduire la fracture numérique: Dans l’esprit du Plan National de lutte
contre la fracture numérique, porté par nos ministres sous les
précédentes législatures, le PS lutte contre l’émergence d’une société
de l’information à deux vitesses en levant les obstacles financiers,
culturels, sociologiques et éducatifs qui empêchent un accès libre et
égal de tous les citoyens aux nouvelles technologies. Concrètement, pour
atteindre ces objectifs, le PS propose de : [...]

.. raw:: html

   </li>

\\r

-  encourager l'utilisation de standards ouverts et des
   logiciels\\rlibres pour garantir un accès libre des citoyens à tous
   les contenus en\\rligne.

\\r

.. raw:: html

   </ul>

\\r

Mais encore:
~~~~~~~~~~~~

\\r

Rappelons enfin que CdH, Ecolo et PS ont signé
`la\\rdéclaration\\rde\\rpolitique régionale wallonne
2009-2014 <\"http://easi.wallonie.be/servlet/Repository/DPR_wallonne_2009.PDF?IDR=9295\">`__
et
`sa\\rcontrepartie\\rcommunautaire <\"http://easi.wallonie.be/servlet/Repository/DPR_communautaire_2009.PDF?IDR=9297\">`__
qui font la part belle aux logiciels libres et aux\\rstandards ouverts.

| \\rVoilà qui est bien pour le logiciel libre et les standards
ouverts,\\ret promet ... si les candidats sont fidèles au programme du
parti, de\\rnombreuses signatures.
| Si vous avez des remarques, commentaires ou compléments, n'hésitez pas
à les envoyer à la `liste de distribution de courriers
électroniques <\"http://www.april.org/wws/info/candidatsbe\">`__. Merci

.. raw:: html

   </p>

