Présentation et discussion aux jeudis du libre de 5 juillet 2012 à 19h
######################################################################
:date: 2012-07-02 20:27
:category: Communales 2012
:slug: presentation-et-discussion-aux-jeudis-du-libre-de-5-juillet-2012-a-19h
:status: published

| Ce jeudi 5 juillet, aux Jeudis du Libre de Bruxelles il sera question
de … `LePacte.be <\"/\">`__  ou comment motiver les candidats aux
élections par rapport aux logiciels libres, à un internet libre et aux
données libres
| \\r
| \\rL'initiative `LePacte.be <\"http://lepacte.be/\">`__ ou les pactes
des libertés numériques sont 3 pactes et engagements en rapport aux
logiciels libres, à un internet libre et aux données libres que 198
candidats dont 37 élus aux élections fédérales de 2010 ont signés.

\\r

Publics concernés par la conférence : tout utilisateur de logiciels
libres, de données libres et d’un internet libre

\\r

Niveau requis : citoyen.

\\rLieu de cette séance : `Betagroup Coworking
Brussels <\"http://coworking.betagroup.be/\">`__ à `ICAB Business &
Technology Incubator <\"http://www.icabrussel.be/\">`__ , 4 rue des
pères blancs – 1040 Bruxelles (cf. `ce plan sur le site
d’Openstreetmap <\"http://www.openstreetmap.org/?lat=50.82584&lon=4.3989&zoom=17&layers=M&mlat=50.82682&mlon=4.40019\">`__).\\r

|  Horaires: de 19h à 21h, suivi d’un restaurant dans le quartier.
| \\r
| \\r>> Merci de vous
`inscrire <\"http://jdl-bruxelles-2012-07.eventbrite.com/\">`__ <<

\\r

Contenu :

\\r

| Présentation des pactes des libertés numériques initiés en 2009 avec
le pacte du logiciel libre aux élections régionales belges, dans la
perspective du pacte des logiciels libres proposé par
`APRIL <\"http://april.org\">`__, aux candidats français. Ce pacte de
2009 a été repris et étendu en 2010 pour les élections fédérales belges,
et concernait alors les logiciels libres, un internet libre (cf
`ACTA <\"http://www.laquadrature.net/acta\">`__) et les données libres
(`open
data <\"http://fr.wikipedia.org/wiki/Donn%C3%A9es_ouvertes\">`__). Cet
exposé revisitera la mise en oeuvre des campagnes, ses actions, ses
résultats (près de la moitié des députés fédéraux francophones élus ont
signé les 3 pactes), mais aussi ses défis et perspectives. La
présentation évoquera la prochaine élection communale du 14 octobre 2012
et les actions à mener pour en faire un succès pour conscientiser plus
les décideurs locaux à ces problématiques importantes à nos yeux.
| \\r
| \\rL’intervenant conférencier : `Nicolas
Pettiaux <\"http://pettiaux.be\">`__ (`et à
APRIL <\"http://www.april.org/fr/nicolas-pettiaux-se-presente\">`__)

\\r

Maître-assistant à la `Haute École de Bruxelles – École Supérieure
d’Informatique <\"http://www.heb.be/esi/\">`__, professeur à l’\ `École
nationale des arts-visuels La Cambre <\"http://www.lacambre.be/\">`__ et
assistant à l’\ `Université Libre de
Bruxelles <\"http://ulb.ac.be/\">`__. Membre du
`BxLUG <\"http://www.bxlug.be/\">`__, de
`APRIL <\"http://april.org/\">`__ et `AFUL <\"http://aful.org/\">`__
dont il a été administrateur. Initiateur des campagnes
`candidats.be <\"http://candidats.be\">`__,
`lepacte.be <\"http://lepacte.be\">`__ et
`hetpact.be <\"http://hetpact.be\">`__. Activiste et utilisateur de
`Debian <\"http://debian.org/\">`__ et
`Ubuntu <\"http://www.ubuntu.com/\">`__ entre autres `logiciels
libres <\"http://fr.wikipedia.org/wiki/Logiciel_libre\">`__.

\\r

Liens :

\\r

-  `lepacte.be <\"http://lepacte.be\">`__
-  `hetpact.be <\"http://hetpact.be\">`__
-  `candidats.be <\"http://candidats.be\">`__
-  `candidats.fr <\"http://candidats.fr\">`__

.. raw:: html

   </p>

