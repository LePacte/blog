Jesigne.lepacte.be : le nouvel outil simple est en place pour tous les candidats
################################################################################
:date: 2012-10-10 09:23
:category: Communales 2012
:slug: jesignelepactebe-le-nouvel-outil-simple-est-en-place-pour-tous-les-candidats
:status: published

Les développeurs du pacte des libertés numériques ont été très actifs
ces derniers jours pour permettre à tous les candidats d'encoder
simplement leur engagement aux 3 pactes.

\\r

Nous avons développé un système en ligne sécurisé et fiable, basé sur le
respect des lois belges qui considèrent comme valides les signatures
autographes ou électroniques avec la carte d'identité électronique.

\\r

Allez simplement en ligne sur
` <\"http://jesigne.lepacte.be\">`__\ `Jesigne.lepacte.be <\"https://jesigne.lepacte.be\">`__\ 
et suivez le mode d'emploi en 2 étapes. Facile, et rapide.

.. raw:: html

   </p>

