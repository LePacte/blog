Victoire de l'internet libre
############################
:date: 2012-07-05 05:43
:category: Free Data
:slug: victoire-de-linternet-libre
:status: published

Ce mercredi 4 juillet 2012, grande victoire de la démocratie au
Parlement européen : le projet ACTA, négocié en secret depuis plusieurs
années, est rejeté par 478 voix contre, 39 voix pour et 165 abstentions.

\\r

Ceci va dans le sens du pacte de l'internet libre de ce chantier.

\\r

Merci à tous ceux qui ont contribué, aux députés et à leurs conseillers
qui ont suivi les attentes des citoyens, en particulier à Jérémie
Zimmerman de La Quadrature du Net.

.. raw:: html

   </p>

