Et encore 3 signataires : la campagne bruxelloise devient multilingue
#####################################################################
:date: 2009-05-31 23:23
:category: Européennes et régionales 2009
:slug: et-encore-3-signataires-la-campagne-bruxelloise-devient-multilingue
:status: published

Ce dimanche 31 mai 2009 fut un jour de marché (comme tous les dimanches)
mais aussi le jour des 20 km de Bruxelles. Une grande foule parcourait
les rues de Bruxelles.

Durant la première partie de la journée, alors que de nombreux candidats
parcouraient le marché proche de chez moi en quête de voix, je le
faisais avec des pactes du logiciel libre. Et j'ai obtenu 3 signatures
de candidats, dont 2 bruxellois néerlandophones. D'où le multilinguisme
qui apparaît dans l'effort candidats.be.

J'ai aussi discuté avec un des ministres de tutelle de la
`Cocof <\"http://www.cocof.irisnet.be/site/fr\">`__ qui n'a rien voulu
signer (par principe de ministre qui ne signe aucune pétition ;-) mais
qui m'a affirmé avoir revu sa position, être devenu «à ce point
favorable au logiciel libre qu'il le recommande dans son cabinet et
l'impose à l'administration». Les conseillers du ministre qui
l'accompagnaient ont essayé de me dire que je ferais bien d'en faire de
même, ce que j'ai quand même trouvé un peu piquant.

.. raw:: html

   </p>

