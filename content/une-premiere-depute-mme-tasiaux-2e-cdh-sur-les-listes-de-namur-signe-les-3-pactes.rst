Une première député, Mme Tasiaux, 2e cdH sur les listes de Namur, signe les 3 pactes.
#####################################################################################
:date: 2010-06-02 20:41
:category: Legislatives 2010
:slug: une-premiere-depute-mme-tasiaux-2e-cdh-sur-les-listes-de-namur-signe-les-3-pactes
:status: published

Et zou, cela va vite. Les pactes à peine terminés et les premiers
courriels de sollicitation envoyés à des candidats, les signatures
reviennent.

Une première député sortante a signé les 3 pactes, du logiciel libre,
d'internet libre et du logiciel libre, Madame Isabelle Tasiaux, 2è sur
la liste cdH à Namur.

Félicitations à elle et merci. Nous espérons qu'elle soit la première
d'une longue série.

.. raw:: html

   </p>

