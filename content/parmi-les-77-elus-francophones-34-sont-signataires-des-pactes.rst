Parmi les 77 élus francophones, 34 sont signataires des Pactes!
###############################################################
:date: 2010-06-14 22:47
:category: Legislatives 2010
:slug: parmi-les-77-elus-francophones-34-sont-signataires-des-pactes
:status: published

Résultats à confirmer une fois que le jeu de chaises musicales se sera
stabilisé mais pour l'instant parmi des 77 élus francophones, `34 sont
signataires <\"/legis2010/index.php?action=liste_signataires&o_sens=ASC&o_nom=1&elus_only=1\">`__
des Pactes, soit 44.2%.

28/62 (45.2%) à la Chambre et 6/15 (40%) au Sénat.

Pas mal du tout!

La palme revient à la circonscription électorale de Namur avec 5
signataires pour 6 élus!

Liège suit avec 9 signataires sur 15 élus.

Côté néerlandophone, Eva Brems à la Chambre et Bert Anciaux au Sénat
sont les deux élus qui ont signé les Pactes, soit 1/88 et 1/25 donc au
total 2 sur 113 (1.8%)

.. raw:: html

   </p>

