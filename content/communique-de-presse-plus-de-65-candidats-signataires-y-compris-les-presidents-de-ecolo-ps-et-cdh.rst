Communiqué de presse : plus de 65 candidats signataires, y compris les présidents de Ecolo, PS et cdH
#####################################################################################################
:date: 2009-06-06 07:56
:category: Européennes et régionales 2009
:slug: communique-de-presse-plus-de-65-candidats-signataires-y-compris-les-presidents-de-ecolo-ps-et-cdh
:status: published

.. raw:: html

   <p>

.. raw:: html

   <title>

Communiqué de presse - candidats.be - 6 juin 2009

.. raw:: html

   </title>

\\r

*Bruxelles, le 6 juin 2009. Communiqué de presse.*\\r

\\r

\\r\ **À l'occasion des élections régionales et européennes de\\rce  7
juin 2009, l'April a lancé récemment une nouvelle campagne\\rsous
la\\rbannière de l'initiative Candidats.be : tous les citoyens attachés
au\\r\ `Logiciel Libre <\"http://www.april.org/fr/node/10307\">`__ sont
invités à proposer** `le Pacte du Logiciel\\rLibre\\raux
candidats <\"http://www.candidats.be/\">`__\ **.\\rLe Pacte du Logiciel
Libre a déjà recueilli plus de 65 signatures, dont\\rles présidents des
partis Ecolo, du cdH et du parti socialiste, de\\rnombreuses têtes de
listes et des ministres sortants. L'April appelle à\\rl'information et à
la mobilisation pour le\\rdernier jour de la campagne.\\r**

\\r

\\r\ `Le\\rPacte du Logiciel
Libre <\"http://candidatsbe.april.org/index.php?pages/Le-pacte-du-logiciel-libre\">`__
est un document simple permettant aux\\rélecteurs de savoir quel
candidat dans leur circonscription a\\rconscience des enjeux du Logiciel
Libre et s'est engagé à promouvoir\\ret défendre les libertés qui y sont
associées.

\\r

\\r\ *« Les Parlements, tant régionaux et communautaires que\\reuropéen,
et les gouvernements qui en sont issus, sont les lieux de\\rdécision et
de discussion de la démocratie. Les députés sont donc
un\\rrelais\\rdémocratique essentiel, et leur sensibilisation aux enjeux
du Logiciel\\rLibre est capitale, »* a déclaré Nicolas
Pettiaux,\\radministrateur belge de l'April.

\\r

Aux échelons régionaux et communautaires, ce sont

\\r

-  les gouvernements qui décident des logiciels qui seront
   installés\\rdans les institutions publiques telles que
   administrations, écoles,\\rhopitaux ... où le choix de l'outil
   informatique créera ou non une\\rdépendance plus ou moins forte avec
   certains fournisseurs, d'autant\\rmoins forte qu'une solution libre
   bien mise en oeuvre dans un cadre\\rprocédural et légal adéquat sera
   choisie,
-  les parlements qui décident de l'environnement légal par
   exemple\\rconcernant la protection des auteurs de logiciels et de
   contenus.

\\r

En particulier, à l'échelon européen, ce sont les eurodéputés qui\\r

\\r

-  ont rejeté la directive brevets en 2005 (danger
   \\"brevets\\rlogiciels\\")
-  ont voté les directives EUCD (danger \\"DRM\\"), IPRED I\\ret II
   (lois sur la contrefaçon, renforcement des droits des détenteurs\\rde
   brevets, criminalisation du pair-à-pair... - dangers
   \\"brevets\\rlogiciels\\" et \\"DRM\\")
-  travaillent actuellement sur le Paquet Télécom, dans lequel
   on\\rassiste à des tentatives d'instauration du filtrage,
   d'imposition de\\rl'informatique de confiance (danger \\"informatique
   déloyale\\") comme\\rnorme de sécurité, ou encore d'atteintes à la
   neutralité de l'Internet\\rpar les opérateurs de télécommunications
-  vont examiner prochainement une directive sur la protection
   des\\rconsommateurs (danger \\"vente liée\\")
-  ont voté le cadre commun d'interopérabilité applicable aux\\rservices
   d'administration électronique (IDABC)

\\r

\\rIl est donc essentiel, pour encourager l'utilisation du logiciel
libre,\\rl'indépendance des institutions publiques dans le
contexte\\rtechnologique, mais aussi pour encourager un développement
économique\\rlocal de compétences informatiques indépendantes en
Belgique, et pour\\rprévenir des discriminations en droit belge contre
les auteurs et\\rutilisateurs de logiciels libres et préserver
l'interopérabilité, de\\rsensibiliser les députées et les eurodéputés à
ces enjeux dès\\rmaintenant.

\\r

Les députés\\reuropéens ont montré récemment leur détermination à
résister aux\\rpressions : en adoptant de nouveau massivement (9 voix
sur 10)\\rl'amendement 138/46 du « Paquet
Telecom »\ `1 <\"http://www.april.org/fr/europeennes-2009-logiciel-libre-des-libertes-a-proteger-une-industrie-a-developper#footnote1_52yf9gw\">`__,\\rqui
rappelle que l'accès à\\rInternet est un moyen essentiel d'exercer des
libertés fondamentales\\rtelles que la liberté d'expression et\\rcelle
de l'accès à l'information, les eurodéputés se sont opposés
aux\\rlobbies du divertissement et à leur projet de riposte graduée.\\r

\\r

\\r\ *« L'Europe est championne du Logiciel Libre, malgré\\rdes
discriminations et l'absence de volonté politique claire.\\rL'Europe
doit prendre en compte ses industries en développement et\\rl'intérêt
des consommateurs. Il est temps de mettre un terme aux abus\\rde
position dominante des géants du logiciel. Le potentiel en
pleine\\rcroissance du Logiciel Libre pourra alors se changer en
atout\\réconomique majeur ! »* a déclaré Frédéric Couchet,\\rdélégué
général de l'April.\\r

\\r

À ce jour le Pacte du Logiciel Libre en Belgique a déjà recueilli\\rplus
de 65\\rsignatures, dont celles présidents\\rdes partis Ecolo, du cdH et
du parti socialiste, de nombreuses têtes de\\rlistes et des ministres
sortants.

\\r

\\rDes têtes de liste notamment du Parti socialiste (PS), d'Ecolo,
du\\rCentre démocrate humaniste (cdH) ont signé le pacte du logiciel
libre.\\rEcolo a aujourdhui le plus de signataires, suivi du cdH puis du
PS.\\r

\\r

Pour le moment un seul candidat du Mouvement réformateur (MR) a\\rsigné
le\\rpacte du logiciel libre.

\\r

\\r\ *« La mobilisation de chacun est essentielle pour que
les\\rcandidats\\rprennent la mesure de l'attachement des citoyens au
Logiciel Libre et\\raux libertés qu'il leur offre. Nous invitons tous
les citoyens\\rattachés au Logiciel Libre à proposer le Pacte
du\\rLogiciel Libre aux candidats de manière à connaître leur
position »*\\ra déclaré Benoît Sibaud, président de l'April.\\r

\\r

La campagne du Pacte du Logiciel Libre se déroule également en\\rFrance,
Italie, Espagne, et au Royaume-Uni, et continue de s'étendre
à\\rd'autres pays
d'Europe.\ `3 <\"http://www.april.org/fr/europeennes-2009-logiciel-libre-des-libertes-a-proteger-une-industrie-a-developper#footnote3_6jo2tlz\">`__\\r

\\r

\\r\ `Retrouvez\\rla liste complète des signataires pour la
Belgique <\"http://candidats.be/europarl2009/?action=liste_signataires&o_sens=DESC*o_date=1\">`__.\\r

\\r

**À propos de l'April**

\\r

Pionnière du logiciel libre en France,
`l'April <\"http://www.april.org\">`__\\rest depuis 1996 un acteur
majeur de la démocratisation et de la\\rdiffusion du logiciel libre et
des standards ouverts auprès du grand\\rpublic, des professionnels et
des institutions dans l'espace\\rfrancophone. Elle veille aussi, dans
l'ère numérique, à sensibiliser\\rl'opinion sur les dangers d'une
appropriation exclusive de\\rl'information et du savoir par des intérêts
privés.

\\r

\\rL'association est constituée de plus plus de 5 000 membres
utilisateurs\\ret\\rproducteurs de logiciels libres dont 223 sociétés ou
réseaux de\\rsociétés,\\r125 associations, 4 collectivités locales,
trois départements\\runiversitaires et une université.\\r

\\r

\\rL'April est l'acteur majeur de la promotion et de la\\rdéfense du
logiciel libre en France.\\r

\\r

Pour plus d'informations, vous pouvez vous rendre sur le site\\rWeb à
l'adresse suivante :
`http://www.april.org/ <\"http://www.april.org/\">`__,\\rnous contacter
en Belgique par\\rtéléphone au +32 496 24 55 01 ou par\\rcourriel à
l'adresse `npettiaux@april.org <\"mailto:npettiaux@april.org\">`__\\rou
au siège français de l'association à l'adresse
`contact@april.org <\"mailto:contact@april.org\">`__.

\\r

Contacts\\rpresse :

\\r

-  Nicolas Pettiaux, administrateur belge de April,
   `npettiaux@april.org <\"mailto:npettiaux@april.org\">`__\\r+32 496 24
   55 01

.. raw:: html

   </p>

