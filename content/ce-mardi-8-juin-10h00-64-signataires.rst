Ce mardi 8 juin 10h00: 64 signataires
#####################################
:date: 2010-06-08 06:46
:category: Legislatives 2010
:slug: ce-mardi-8-juin-10h00-64-signataires
:status: published

La situation ce matin: 64 signataires

\\r

Ici-bas les détails par parti et par pacte :

-  `Ecolo <\"?action=liste_candidats&parti=20\">`__ (`35 <\"?action=liste_signataires&parti=20\">`__) dont
   les co-présidents 
-  `Parti
   socialiste <\"?action=liste_candidats&parti=12\">`__ (`9 <\"?action=liste_signataires&parti=12\">`__) dont
   le président 
-  `Mouvement
   réformateur <\"?action=liste_candidats&parti=18\">`__ (`8 <\"?action=liste_signataires&parti=18\">`__) dont
   le président 
-  `Centre démocrate
   humaniste <\"?action=liste_candidats&parti=13\">`__ (`6 <\"?action=liste_signataires&parti=13\">`__) dont
   la présidente 
-  `Pirate
   Party <\"?action=liste_candidats&parti=26\">`__ (`3 <\"?action=liste_signataires&parti=26\">`__)
    dont le président 
-  `Pro
   Bruxsel <\"?action=liste_candidats&parti=25\">`__ (`2 <\"?action=liste_signataires&parti=25\">`__)
-  `Groen
   ! <\"?action=liste_candidats&parti=16\">`__ (`1 <\"?action=liste_signataires&parti=16\">`__) dont
   le président 

\\r

Les détails des signataires sont visibles \ `ici. <\"/legis2010/\">`__ 

\\r

En général les candidats ont signé les 3 pactes et lorsque ce n'est pas
le cas c'est la grande majorité du temps à cause d'un oubli et les
candidats sont en cours de régularisation. 

\\r

Nous en sommes donc ce matin à: 

\\r

-  64 signataires du \ `Pacte des Logiciels
   Libres <\"http://www.candidats.be/index.php?pages/Le-pacte-du-logiciel-libre\">`__
-  62 signataires du \ `Pacte des Données
   Libres <\"http://lepacte.be/?pages/Le-pacte-des-donnees-libres-2010\">`__
-  62 signataires du \ `Pacte de l'Internet
   Libres <\"http://lepacte.be/?pages/Le-pacte-de-l-internet-libre-2010\">`__

.. raw:: html

   </p>

