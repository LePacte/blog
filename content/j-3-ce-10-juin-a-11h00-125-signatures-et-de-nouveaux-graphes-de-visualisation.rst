J-3 : Ce 10 juin à 11h:00, 125 signatures et de nouveaux graphes de visualisation
#################################################################################
:date: 2010-06-10 04:35
:category: Legislatives 2010
:slug: j-3-ce-10-juin-a-11h00-125-signatures-et-de-nouveaux-graphes-de-visualisation
:status: published

De nombreux candidats de tous les partis démocratiques francophones
ont\\rsigné hier les pactes des libertés numériques : nous sommes passés
de 64\\rsignatures à 125 en quelques heures. Près du double.

\\r

Merci à tous de\\rreconnaître l'importance de ces thèmes et de vous
engager par rapport à\\rceux-ci.

| Nos équipes ne chôment pas non plus, pour encoder en temps réel ou
presque, les signatures reçues, répondre aux candidats et les remercier,
mais aussi poursuivre les développements de l'outil de suivi. Sur la
`page des graphes <\"legis2010\">`__ vous pouvez en effet maintenant
trouver des graphes actualisés en permanence.\\r
| \\r\ **La situation ce 10 juin à 11h00 est la suivante:**\\r

| \\r\\r

.. raw:: html

   <table>

\\r

.. raw:: html

   <caption>

2010 Digital Freedom Pacts

.. raw:: html

   </caption>

\\r

.. raw:: html

   <thead>

\\r

.. raw:: html

   <tr>

\\r

.. raw:: html

   <td>

| 

.. raw:: html

   </td>

\\r

.. raw:: html

   <th scope="\&quot;col\&quot;">

Free Data

.. raw:: html

   </th>

\\r

.. raw:: html

   <th scope="\&quot;col\&quot;">

Free Software

.. raw:: html

   </th>

\\r

.. raw:: html

   <th scope="\&quot;col\&quot;">

Free Internet

.. raw:: html

   </th>

\\r

.. raw:: html

   </tr>

\\r

.. raw:: html

   </thead>

\\r

.. raw:: html

   <tbody>

\\r

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

Ecolo

.. raw:: html

   </th>

.. raw:: html

   <td>

40

.. raw:: html

   </td>

.. raw:: html

   <td>

41

.. raw:: html

   </td>

.. raw:: html

   <td>

40

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

cdH

.. raw:: html

   </th>

.. raw:: html

   <td>

42

.. raw:: html

   </td>

.. raw:: html

   <td>

41

.. raw:: html

   </td>

.. raw:: html

   <td>

41

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

PS

.. raw:: html

   </th>

.. raw:: html

   <td>

19

.. raw:: html

   </td>

.. raw:: html

   <td>

18

.. raw:: html

   </td>

.. raw:: html

   <td>

19

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

MR

.. raw:: html

   </th>

.. raw:: html

   <td>

10

.. raw:: html

   </td>

.. raw:: html

   <td>

10

.. raw:: html

   </td>

.. raw:: html

   <td>

10

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

Pirate Party

.. raw:: html

   </th>

.. raw:: html

   <td>

6

.. raw:: html

   </td>

.. raw:: html

   <td>

6

.. raw:: html

   </td>

.. raw:: html

   <td>

6

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

Pro Bruxsel

.. raw:: html

   </th>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   <tr>

.. raw:: html

   <th scope="\&quot;row\&quot;">

Groen

.. raw:: html

   </th>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   <td>

3

.. raw:: html

   </td>

.. raw:: html

   </tr>

.. raw:: html

   </tbody>

\\r

.. raw:: html

   </table>

| \\r
| \\r
| \\r\ |image0|\\r
| \\r\ |image1|

.. raw:: html

   </p>

.. |image0| image:: \"legis2010/graph/chart201006101100a.png\"
.. |image1| image:: \"legis2010/graph/chart201006101100b.png\"
