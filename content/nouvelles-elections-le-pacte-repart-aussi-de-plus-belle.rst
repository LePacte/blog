Nouvelles élections, le pacte repart aussi, de plus belle.
##########################################################
:date: 2012-06-26 19:18
:category: Communales 2012
:slug: nouvelles-elections-le-pacte-repart-aussi-de-plus-belle
:status: published

Le 14 octobre 2012 prochain, les belges sont appelés à élire leurs
conseils communaux. Nombreux seront les candidats locaux aux fonctions
de bourgmestres, échevins et conseillers communaux.

\\r

Cet échelon local de la vie politique est essentiel, il draine des
marchés notamment informatiques importants et emploie de très nombreuses
personnes. Il nous paraît donc essentiel de sensibiliser les candidats
de tous les partis aux questions des logiciels libres, des données
ouvertes et de l'internet libre, un sujet particulièrement chaud à la
veille du vote à propos de ACTA de ce 4 juillet 2012 au Parlement
européen.

\\r

Nous préparons aussi 1 nouveau pacte complémentaire que nous proposerons
aussi à tous les 193 candidats aux précédentes élections fédérales,
signataires des pactes des libertés numériques, dont 34 sur 77 sont des
élus. Les nouveaux auront droit à un panel de 4 pactes des libertés
numériques.

\\r

Enfin, pour mieux nous organiser, nous nous y prenons plus tôt et
faisons un exposé ce jeudi 5 juillet 2012 à 19h00 au Beta Coworking
group, dans le cadre des jeudis du libre de Bruxelles. Nous espérons
vous y voir nombreux.

\\r

Nous vous proposerons aussi très bientôt le traditionnel pot du pacte
pour commencer les festivités.

.. raw:: html

   </p>

