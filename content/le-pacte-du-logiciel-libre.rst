Le Pacte du Logiciel Libre
##########################
:date: 2010-06-03 07:18
:slug: le-pacte-du-logiciel-libre
:status: published

| Le Pacte du Logiciel Libre 2010 est une initiative belge de
l'\ `April <\"http://april.org\">`__ soutenue par l'a.s.b.l. `À l'Ère
Libre <\"http://alerelibre.be\">`__.
| Le site officiel de cette initiative, dont la coordination est faite
ici, est `Candidats.be <\"http://candidats.be\">`__, vous y retrouverez
les billets et l'information relatifs au Logiciel Libre.
| **Le Pacte du Logiciel Libre est directement accessible** `en suivant
ce
lien <\"http://www.candidats.be/index.php?pages/Le-pacte-du-logiciel-libre\">`__\ **.**
