Les signataires des pactes principaux en 2010
#############################################
:date: 2012-10-10 09:06
:category: Legislatives 2010
:slug: les-signataires-des-pactes-principaux-en-2010
:status: published

Par ordre alphabétique des noms de partis francophones principaux, et
dans ces listes par ordre alphabétique des noms, voici les candidats qui
ont signés les pactes des libertés numériques à l'occasion des élection
de juin 2010, et la date à laquelle ils ont envoyé les pactes signés.

\\r

Si vous êtes à nouveau candidat en 2012, il est utile de resigner le
pacte (ce qui est maintenant très simple) pour être repris dans notre
nouveau tableau et valoriser le nombre total de signataires de votre
parti.

\\r

Tous les 157 candidats signataires
----------------------------------

\\r

71 signataires Centre démocrate humaniste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Nese Acikgöz (11/06/10)
| 2.  Lise Amorison (12/06/10)
| 3.  Najyha Aynaou (11/06/10)
| 4.  Christophe Bastin (09/06/10)
| 5.  Jean-Paul Bastin (09/06/10)
| 6.  Christian Brotcorne (09/06/10)
| 7.  Danielle Caron (09/06/10)
| 8.  Idès Cauchie (10/06/10)
| 9.  Benoît Cerexhe (10/06/10)
| 10.  Caroline Charpentier (11/06/10)
| 11.  André Cobbaert (11/06/10)
| 12.  René Collin (07/06/10)
| 13.  Justine Comijn (09/06/10)
| 14.  Claudine Coolsaet (08/06/10)
| 15.  Annie Cotton (09/06/10)
| 16.  Hélène Couplet-Clément (09/06/10)
| 17.  Georges Dallemagne (09/06/10)
| 18.  Nathalie de Suray (09/06/10)
| 19.  Elisabeth Degryse (09/06/10)
| 20.  Myriam Delacroix (11/06/10)
| 21.  Francis Delpérée (09/06/10)
| 22.  Anne Delvaux (10/06/10)
| 23.  Carlo Di Antonio (06/06/10)
| 24.  André Du Bus De Warnaffe (11/06/10)
| 25.  Jean-François Favresse (09/06/10)
| 26.  Mohamed Fekrioui (11/06/10)
| 27.  Michel Firket (09/06/10)
| 28.  Catherine Fonck (09/06/10)
| 29.  Joseph George (08/06/10)
| 30.  Vicent Girboux (11/06/10)
| 31.  Mélanie Goffin (09/06/10)
| 32.  Marie-Eve Hannard (11/06/10)
| 33.  Maurice Jennequin (09/06/10)
| 34.  Olivier Joris (11/06/10)
| 35.  Myriam Lacroix (11/06/10)
| 36.  Benoit Langendries (11/06/10)
| 37.  Stéphane Lasseaux (09/06/10)
| 38.  David Lavaux (09/06/10)
| 39.  Aline Leclercq (11/06/10)
| 40.  Gérard Lemaire (09/06/10)
| 41.  Chloé Liessens (09/06/10)
| 42.  Claire Lobet-Maris (05/06/10)
| 43.  Benoît Lutgen (11/06/10)
| 44.  Philippe Matthis (11/06/10)
| 45.  Vanessa Matz (10/06/10)
| 46.  Solange Mesdagh (09/06/10)
| 47.  Pierre Migisha (11/06/10)
| 48.  Joëlle Milquet (03/06/10)
| 49.  Olivier Moinnet (11/06/10)
| 50.  Yves Moulin (09/06/10)
| 51.  Antoine Nivard (06/06/10)
| 52.  Clotilde Nyssens (11/06/10)
| 53.  Maxime Prevot (10/06/10)
| 54.  Jean-Paul Procureur (09/06/10)
| 55.  Sylvie Roberti (08/06/10)
| 56.  Rodolphe Sagehomme  (09/06/10)
| 57.  Véronique Salvi (11/06/10)
| 58.  Marie-Dominique Simonet (08/06/10)
| 59.  Evelyne Stinglhamber-Van Pee (09/06/10)
| 60.  Antoine Tanzilli (17/06/10)
| 61.  Isabelle Tasiaux (02/06/10)
| 62.  Benoit Thoreau (09/06/10)
| 63.  Aurore Tourneur (09/06/10)
| 64.  Cécile van Hecke (10/06/10)
| 65.  Catherine van Zeeland (11/06/10)
| 66.  Annick Van Den Ende (10/06/10)
| 67.  Michaël Van Hooland (11/06/10)
| 68.  Olivier Vanham (09/06/10)
| 69.  Melchior Wathelet (09/06/10)
| 70.  Brigitte Wiaux (11/06/10)
| 71.  Damien Winance (08/06/10)

\\r

62 signataires Ecolo
~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Willy Bernimolin (10/06/10)
| 2.  Annick Boidron (04/06/10)
| 3.  Juliette Boulet (07/06/10)
| 4.  Saskia Bricmont (04/06/10)
| 5.  Anne-Marie Camus (08/06/10)
| 6.  Grégory Cardarelli (04/06/10)
| 7.  Pierre Castelain (10/06/10)
| 8.  Thierry Catteau (08/06/10)
| 9.  Cécile Cornet (10/06/10)
| 10.  Sandrine Couturier (07/06/10)
| 11.  José Daras (04/06/10)
| 12.  Cécile Dascotte (08/06/10)
| 13.  Charlotte de Jaer (07/06/10)
| 14.  Olivier Deleuze (03/06/10)
| 15.  Ingrid Delmot (10/06/10)
| 16.  Chloé Deltour (09/06/10)
| 17.  Christina Dewart (09/06/10)
| 18.  Anne Dister (10/06/10)
| 19.  Hugues Doumont (11/06/10)
| 20.  Thomas Eraly (07/06/10)
| 21.  Tamimount Essaïdi (10/06/10)
| 22.  Jean-François Fauconnier (04/06/10)
| 23.  Gian-Pietro Favarin (10/06/10)
| 24.  Zoé Genot (04/06/10)
| 25.  Muriel Gerkens (11/06/10)
| 26.  Christine Ghys (10/06/10)
| 27.  Georges Gilkinet (10/06/10)
| 28.  Christian Grétry (07/06/10)
| 29.  Benoît Hellings (04/06/10)
| 30.  Laurence Hennuy (07/06/10)
| 31.  Anne Herscovici (07/06/10)
| 32.  Evelyne Huytebroeck (10/06/10)
| 33.  Jean-Michel Javaux (03/06/10)
| 34.  Bob (Kazadi) Kabamba (07/06/10)
| 35.  Catherine Kestelyn (07/06/10)
| 36.  Fouad Lahssani (10/06/10)
| 37.  Michaël Leclercq (11/06/10)
| 38.  Marie-Christine Lefebvre (06/06/10)
| 39.  Jean-Marc Monin (10/06/10)
| 40.  Jacky Morael (11/06/10)
| 41.  Catherine Morenville (07/06/10)
| 42.  Marie Nagy (10/06/10)
| 43.  Agnès Namurois (10/06/10)
| 44.  Antoine Nélisse (07/06/10)
| 45.  Julie Papazoglou (10/06/10)
| 46.  Ingrid Parmentier (07/06/10)
| 47.  Luc Parmentier (10/06/10)
| 48.  André Peters (04/06/10)
| 49.  Jérôme Petit (11/06/10)
| 50.  Daniel Petit (07/06/10)
| 51.  Brigitte Pétré (10/06/10)
| 52.  Eric Remacle (08/06/10)
| 53.  Michel Renard (03/06/10)
| 54.  Pascal Rigot (04/06/10)
| 55.  Jean-Luc Roland (11/06/10)
| 56.  Geoffrey Roucourt (07/06/10)
| 57.  François Ruelle (07/06/10)
| 58.  Brigitte Simal (11/06/10)
| 59.  Thérèse Snoy (04/06/10)
| 60.  Cécile Thibaut (07/06/10)
| 61.  Sarah Turine (03/06/10)
| 62.  Laurence Willemse (07/06/10)

\\r

12 signataires Mouvement réformateur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Alain Courtois (05/06/10)
| 2.  Alain Destexhe (08/06/10)
| 3.  Bernard Ducoffre (08/06/10)
| 4.  Michèle Hasquin-Nahum (08/06/10)
| 5.  Pierre-Yves Jeholet (07/06/10)
| 6.  Marion Lemesre (05/06/10)
| 7.  Benoît Piedboeuf (06/06/10)
| 8.  Didier Reynders (04/06/10)
| 9.  Françoise Schepmans (12/06/10)
| 10.  Dominique Tilmans (09/06/10)
| 11.  David Weytsman (05/06/10)

\\r

35 signataires Parti socialiste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Benoît Bayenet (12/06/10)
| 2.  Véronique Biordi (11/06/10)
| 3.  Carmen Castellano (07/06/10)
| 4.  Dominique Clajot (11/06/10)
| 5.  Guy Coeme (08/06/10)
| 6.  David Cordonnier (10/06/10)
| 7.  Philippe Courard (09/06/10)
| 8.  Michel Daerden (07/06/10)
| 9.  Jean-Marc Delizée (11/06/10)
| 10.  Willy Demeyer (08/06/10)
| 11.  Rudy Demotte (29/06/10)
| 12.  Valérie Déom (09/06/10)
| 13.  Elio Di Rupo (03/06/10)
| 14.  Julie Fiszman (09/06/10)
| 15.  André Flahaut (07/06/10)
| 16.  Catherine François (11/06/10)
| 17.  André Frédéric (10/06/10)
| 18.  Thierry Giet (10/06/10)
| 19.  Gwénaëlle Grovonius (17/06/10)
| 20.  Jamal Ikazban (11/06/10)
| 21.  Rachel Izizaw (10/06/10)
| 22.  Karine Lalieux (16/06/10)
| 23.  Marie-Claire Lambert (10/06/10)
| 24.  Dimitri Legasse (10/06/10)
| 25.  Rachid Madrane (05/06/10)
| 26.  Paul Magnette (10/06/10)
| 27.  Philippe Mahoux (10/06/10)
| 28.  Madeleine Mairlot (10/06/10)
| 29.  Alain Mathot (08/06/10)
| 30.  Vittorio Mettewie (10/06/10)
| 31.  Laurette Onkelinx (07/06/10)
| 32.  Vincent Sampaoli (10/06/10)
| 33.  Sandra Schrauben (10/06/10)
| 34.  Eliane Tillieux (10/06/10)
| 35.  Christiane Vienne (09/06/10)

\\r

7 signataires Pirate Party
~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Delphine Chevalier (09/06/10)
| 2.  Julien Deswaef (04/06/10)
| 3.  Gilles Douillet (10/06/10)
| 4.  Marouan El Moussaoui (09/06/10)
| 5.  Mahdiya El-Ouiali (09/06/10)
| 6.  Monika Kornacka (07/06/10)
| 7.  Jurgen Rateau (04/06/10)

\\r

Et les 35 élus
--------------

\\r

10  signataires Centre démocrate humaniste élus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Christian Brotcorne (09/06/10)
| 2.  Benoît Cerexhe (10/06/10)
| 3.  Francis Delpérée (09/06/10)
| 4.  Catherine Fonck (09/06/10)
| 5.  Benoît Lutgen (11/06/10)
| 6.  Vanessa Matz (10/06/10)
| 7.  Joëlle Milquet (03/06/10)
| 8.  Maxime Prevot (10/06/10)
| 9.  Marie-Dominique Simonet (08/06/10)
| 10.  Melchior Wathelet (09/06/10)

\\r

7 signataires Ecolo élus
~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Juliette Boulet (07/06/10)
| 2.  Olivier Deleuze (03/06/10)
| 3.  Zoé Genot (04/06/10)
| 4.  Muriel Gerkens (11/06/10)
| 5.  Georges Gilkinet (10/06/10)
| 6.  Jacky Morael (11/06/10)
| 7.  Thérèse Snoy (04/06/10)

\\r

3 signataires Mouvement réformateur élus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Sabine Laruelle (10/06/10)
| 2.  Didier Reynders (04/06/10)
| 3.  Dominique Tilmans (09/06/10)

\\r

15 signataires Parti socialiste élus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\\r

| 1.  Guy Coeme (08/06/10)
| 2.  Philippe Courard (09/06/10)
| 3.  Michel Daerden (07/06/10)
| 4.  Jean-Marc Delizée (11/06/10)
| 5.  Willy Demeyer (08/06/10)
| 6.  Rudy Demotte (29/06/10)
| 7.  Valérie Déom (09/06/10)
| 8.  Elio Di Rupo (03/06/10)
| 9.  André Flahaut (07/06/10)
| 10.  André Frédéric (10/06/10)
| 11.  Marie-Claire Lambert (10/06/10)
| 12.  Paul Magnette (10/06/10)
| 13.  Alain Mathot (08/06/10)
| 14.  Laurette Onkelinx (07/06/10)
| 15.  Christiane Vienne (09/06/10)

.. raw:: html

   </p>

