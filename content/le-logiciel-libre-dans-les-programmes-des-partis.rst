Le Logiciel Libre dans les programmes des partis
################################################
:date: 2010-05-16 10:12
:category: Free Software
:slug: le-logiciel-libre-dans-les-programmes-des-partis
:status: published

Ce 16 mai 2010, nous avons parcouru quelques programmes disponibles sur
le réseau.

\\r

Pour le cdH :
~~~~~~~~~~~~~

\\r

Le nouveau programme 2010 n'est pas encore en ligne. Dans le\\rprogramme
des régionales 2009 on retrouvait dans `le\\r3ème axe, Ch. 3)  Agir
contre la fracture
numérique <\"http://www.lecdh.be/sites/default/files/programme/programme-bxl-axe3.pdf\">`__:

\\r

-  Le cdH propose de ... poursuivre une politique concertée visant\\rà
   faciliter l’accessibilité aux nouvelles technologies ...
   en\\rencourageant la diffusion de logiciels gratuits.

| \\rIl est dommage que le cdH confonde logiciels gratuits et logiciels
libres. Ces derniers, par leurs licences, donnent en effet des garanties
et des libertés essentielles aux utilisateurs. `Grégoire
Vincke <\"http://www.fundp.ac.be/universite/personnes/page_view/01005794/\">`__,
membre de `Facultés universitaires Notre Dame de la Paix à
Namur <\"http://www.fundp.ac.be/\">`__ explique clairement `pourquoi
privilégier les logiciels libres en
éducation <\"http://apprendre2point0.ning.com/profiles/blogs/pourquoi-privilegier-les\">`__,
une analyse qui s'étend d'ailleurs en dehors de ce contexte et en
particulier pour toute institution publique.\\r
| Malgré ce défaut de reconnaissance explicite de l'importance des
logciels libres et de leurs libertés dans le programme de parti, de
nombreux candidats et ensuite ministres élus, dont la `présidente Joëlle
Milquet <\"http://candidats.be/europarl2009/?action=editer_candidat&id=134\">`__,
ont signé le pacte du logiciel libre en 2009.\\rLe cdH avait aussi signé
avec le PS et Ecolo `la\\rdéclaration de politique régionale wallonne
2009-2014 <\"http://easi.wallonie.be/servlet/Repository/DPR_wallonne_2009.PDF?IDR=9295\">`__
et `sa\\rcontrepartie
communautaire <\"http://easi.wallonie.be/servlet/Repository/DPR_communautaire_2009.PDF?IDR=9297\">`__
qui font la part belle aux logiciels libres et aux standards ouverts.\\r

Pour Ecolo :
~~~~~~~~~~~~

\\r

Le `nouveau programme
2010 <\"http://web4.ecolo.be/?Notre-programme-2010\">`__ n'est pas
encore en ligne mais renvoit au `programme de
2009 <\"http://web4.ecolo.be/?-Le-programme-electoral-\">`__. Très bien
car ce programme de 2009 dans la section sur la `société de
l'information <\"http://web4.ecolo.be/?Priorite-no4-garantir-une-societe\">`__
propose

\\r

-  de systématiser l’utilisation des logiciels libres de droits dans le
   champ public (pouvoirs publics, administrations, écoles …).

\\rEcolo utilise de plus en plus de logiciels libres en interne selon
nos informations et a signé avec le PS et le cdH `la\\rdéclaration\\rde
politique régionale wallonne
2009-2014 <\"http://easi.wallonie.be/servlet/Repository/DPR_wallonne_2009.PDF?IDR=9295\">`__
et
`sa\\rcontrepartie\\rcommunautaire <\"http://easi.wallonie.be/servlet/Repository/DPR_communautaire_2009.PDF?IDR=9297\">`__
qui font la part belle aux logiciels libres et aux\\rstandards ouverts.

Pour le MR :
~~~~~~~~~~~~

\\r

Le nouveau programme 2010 n'est pas encore en ligne. Dans le `programme
des législatives
2007 <\"http://www.mr.be/media/pdf/programme2007.pdf\">`__, la seule
trace faisant vaguement penser aux standards ouverts est:

\\r

-  Nous proposons ... Que les autorités publiques innovent en
   construisant un e-gouvernement réellement conçu en fonction des
   attentes et des besoins des citoyens et des entreprises. Cela
   implique  ... Les différents niveaux de pouvoirs (fédéral, régional,
   communautaire) doivent davantage travailler en commun, sur base de
   langages d’échanges informatiques identiques.

\\r

Et rien quant aux logiciels libres. Dommage.

\\r

Pour le PS :
~~~~~~~~~~~~

\\r

En page page 132 du `programme\\r2010
téléchargeable <\"http://www.ps.be/_iusr/programme_2010___version_finale.pdf\">`__,
on peut lire

\\r

-  encourager l'utilisation de standards ouverts et des
   logiciels\\rlibres pour garantir un accès libre des citoyens à tous
   les contenus en\\rligne.

| \\rVoilà qui est bien pour le logiciel libre et les standards
ouverts,\\ret promet ... si les candidats sont fidèles au programme du
parti, de\\rnombreuses signatures.
| Ce n'est donc pas la permière fois que le sujet des logiciels libres
figure dans le programme du PS qui se rend compte de leur importance.
| Le PS a signé avec le Ecolo et le cdH `la\\rdéclaration\\rde politique
régionale wallonne
2009-2014 <\"http://easi.wallonie.be/servlet/Repository/DPR_wallonne_2009.PDF?IDR=9295\">`__
et
`sa\\rcontrepartie\\rcommunautaire <\"http://easi.wallonie.be/servlet/Repository/DPR_communautaire_2009.PDF?IDR=9297\">`__
qui, nous ne le disons que pour la troisième fois,  font la part belle
aux logiciels libres et aux\\rstandards ouverts.

En conclusion
~~~~~~~~~~~~~

\\r

Voici les informations que nous avons pu recueillir pour le moment, mais
nous sommes sûrs que bientôt nous aurons des infos\\rplus précises à
vous communiquer encore.

\\r

Nous pouvons saluer les mentions explicites concernant le logiciel libre
dans les programmes du PS et de Ecolo. Dans ceux du cdH, cela doit être
un oubli temporaire vu la signature du pacte 2009 par de nombreux
candidats et élus. 

\\r

Les mentions, par rapport à l'année passée dans les `programmes 2009
pour les européennes et les
régionales <\"http://candidatsbe.april.org/index.php?post/2009/06/05/Dans-les-programmes-des-partis-ou-les-r%C3%A9ponses-%3A-%C3%89colo%2C-PS-et-MR\">`__,
font preuve de cohérence. Reste à voir si cela rentre dans les faits et
est mis en œuvre dans les politiques. Nous y veillerons et le
rappellerons aux signataires.

\\r

Si vous avez des remarques, commentaires ou compléments, n'hésitez pas à
les envoyer à la `liste de distribution de courriers
électroniques <\"http://www.april.org/wws/info/candidatsbe\">`__. Merci

.. raw:: html

   </p>

