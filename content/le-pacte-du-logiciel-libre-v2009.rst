Le pacte du logiciel libre v2009
################################
:date: 2009-06-03 19:42
:slug: le-pacte-du-logiciel-libre-v2009
:status: published

Ce document est aussi téléchargeable en version pdf sur `le pacte du
logiciel libre 2009 en
pdf <\"http://candidatsbe.april.org/public/Pacte_du_logiciel_libre_Fr_be.pdf\">`__
ou en version odt éditable sur `le pacte du logiciel libre 2009 en
odt <\"http://candidatsbe.april.org/public/Free-Software-Pact_FR_be.odt\">`__
avec `la suite bureautique libre et gratuite
OpenOffice.org <\"http://fr.openoffice.org\">`__.

Un bien commun à protéger et à développer.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Je suis candidat(e) aux élections régionales ou européennes 2009
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

J'ai conscience que
^^^^^^^^^^^^^^^^^^^

-  Le travail des acteurs du Logiciel Libre participe à la préservation
   des libertés fondamentales à l'ère du numérique, au partage du savoir
   et à la lutte contre la «;fracture numérique». Il constitue également
   une opportunité pour le public mais aussi pour l'indépendance
   technologique et la compétitivité de nos régions, de nos communautés,
   de la Belgique et de l'Europe;

-  Le Logiciel Libre est un bien commun à protéger et à développer. Son
   existence repose sur le droit pour un auteur de divulguer son
   logiciel avec son code source et d'accorder à tous le droit de les
   utiliser, les copier, les adapter et les redistribuer, en version
   originale ou modifiée.

Je m'engage donc à
^^^^^^^^^^^^^^^^^^

-  Faire développer et utiliser prioritairement des logiciels libres et
   des standards ouverts dans les établissements, administrations et
   services placés sous ma compétence ou gérés par des partenaires ;

-  Accompagner systématiquement ces initiatives en sensibilisant aux
   logiciels libres et aux standards ouverts les publics amenés à en
   bénéficier ;

-  Défendre les droits des auteurs et des utilisateurs de logiciels
   libres, notamment en demandant la modification de toute disposition
   légale fragilisant ces droits et en m'opposant à tout projet ou
   proposition les réduisant.

**Nom:**

**Prénom:**

Candidat au Parlement : européen / wallon / flamand / bruxellois /
germanophone (biffer la mention inutile)

Liste / parti et place sur la liste :

*Coordonnées de confirmation:*

Adresse:

Code postal:

Ville:

Téléphones:

Courriel:

Fax:

Fait le:

À:

Signature:

À renvoyer par courrier ou par fax à :

***April , c/o Nicolas Pettiaux, Avenue du pérou 29 – 1000 Bruxelles,
Tél : +32 496 24 55 01 – Fax : + 33 1 45 65 32 90***

Si vous disposez d'un scanner, vous pouvez également renvoyer ce pacte
signé et numérisé à `pacte \_\_at\_\_
candidats.be <\"mailto:%70%61%63%74%65%20%5f%5f%61%74%5f%5f%20%63%61%6e%64%69%64%61%74%73%2e%62%65\">`__

.. raw:: html

   </p>

