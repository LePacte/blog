Parmi d'autres libertés numériques à défendre, les droits du citoyen face au vote électronique
##############################################################################################
:date: 2010-06-04 22:19
:category: Legislatives 2010
:slug: parmi-dautres-libertes-numeriques-a-defendre-les-droits-du-citoyen-face-au-vote-electronique
:status: published

Les élections fédérales anticipées du 13 juin 2010 nous ont pris au
dépourvu comme pas mal de monde.

\\r

Faute de temps, nous n'avons pas pu intégrer certaines initiatives qui
défendent d'autres valeurs liées aux libertés numériques telles que
celle dont nous souhaitons parler ici: défendre les droits du citoyen
face au vote électronique.

Les élections fédérales anticipées du 13 juin 2010 nous ont pris au
dépourvu comme pas mal de monde.

\\r

| Faute de temps, nous n'avons pas pu intégrer\\rcertaines initiatives
qui défendent d'autres valeurs liées aux libertés numériques telles que
celle dont nous souhaitons parler ici: défendre les droits du citoyen
face au vote électronique.
| L'association PourEVA (Pour une Ethique du Vote Automatisé) est une
association de fait, indépendante\\rde tout parti politique, regroupant
des citoyens qui contestent le\\rsystème du vote automatisé tel qu’il se
pratique actuellement en\\rBelgique.\\rIls refusent ce système car il
prive les électeurs de toute possibilité\\rde contrôle sur les élections
auxquelles ils sont appelés à participer.
| L’association PourEVA compte un taux exceptionnellement
élevé\\rd’informaticiens, particulièrement sensibilisés aux dangers
que\\rreprésente le système de vote et de dépouillement utilisé
actuellement.
| Pour eux, ce n'est pas le logiciel mais les élections qui doivent être
libres, mais aussi honnêtes et transparentes.
| L'informatique prive les citoyens électeurs du contrôle des élections.
| Quant à l'utilisation de la carte d'identité électronique ou du vote
par internet, elles ne peut garantir le secret du vote.
| Actuellement, les seules instances habilitées par le ministère
de\\rl'intérieur à contrôler les élections sont des firmes privées et un
collège d'experts choisi par\\rle pouvoir en place.
| \\r
| PourEVA nous propose `quelques
idées <\"http://www.poureva.be/spip.php?article638\">`__ pour défendre
la démocratie ce 13 juin 2010.

.. raw:: html

   </p>

