Vlaamse verkiezingsprogramma's 2010: op zoek naar Digitale Vrijheid
###################################################################
:date: 2010-06-05 17:06
:category: Legislatives 2010
:slug: vlaamse-verkiezingsprogrammas-2010-op-zoek-naar-digitale-vrijheid
:status: published

Zoals voor de programma's van de franstalige partijen al is gebeurd
(`Vrije
Data <\"/?post/2010/05/31/Les-donn%C3%A9es-gouvernementales-dans-les-programmes-des-partis\">`__,
`Vrije
Software <\"http://www.candidats.be/index.php?post/2010/05/31/Dans-les-programmes-des-partis-II\">`__),
gaan we op zoek naar wat de Vlaamse politici te zeggen hebben over Vrije
Software, een Vrij Internet en Vrije Data. De oogst is redelijk mager,
maar er zijn talrijke aanknopingspunten.

`Programma Groen! <\"http://groen.be/uploads/programma/Groen_verkiezingsprogramma_2010.pdf\">`__
================================================================================================

\\r

    64: (Armoede:) We werken drempels in het onderwijs weg.
    `... <\"...\">`__ Ook wil Groen! meer middelen in lokale projecten
    om de digitale kloof te dichten, zodat ook mensen in armoede en hun
    kinderen toegang hebben tot informatie via internet.
    `... <\"...\">`__

    \\r

\\r

Vrije software past perfect in dit plaatje, zoals ook blijkt uit
bestaande initiatieven in bibliotheken en in het onderwijs.

\\r

    30: `... <\"...\">`__ Groene ICT `... <\"...\">`__

    \\r

    174: `... <\"...\">`__ Informatisering justitie `... <\"...\">`__

    \\r

\\r

Ook hierin kan Vrije Software een belangrijke rol spelen, de gebruikers
ervan zijn namelijk niet aan de auteurs van de software gebonden en
kunnen veel flexibeler optreden.

\\r

    5: Zoveel mogelijk diensten worden gedigitaliseerd en online
    beschikbaar (bijv. milieuvergunningen). Er komen centrale
    gegevensdatabanken `... <\"...\">`__

    \\r

    17: We versterken het Globaal Medisch dossier. We willen dat de
    relevante informatie uit het globaal medisch dossier in de toekomst
    beschikbaar is voor andere zorgverstrekkers die de patiënt
    behandelen. Bovendien willen we werk maken van het elektronisch
    medisch dossier, dat het registreren en uitwisselen van gegevens
    sterk zal bevorderen.

    \\r

\\r

Open standaardformaten zijn het meest geschikt om deze doelen te
bereiken. De federale overheid kiest reeds voor
`OpenDocument-formaten <\"http://en.wikipedia.org/wiki/OpenDocument_adoption#Belgium\">`__
voor interne en externe communicatie.\\rDe nuttigste manier om
(niet-persoonsgerelateerde) digitale gegevens aan te bieden is in de
vorm van Vrije Data, dit maakt creatieve toepassingen van de informatie
van de overheid mogelijk. Zie bijvoorbeeld
`hier <\"http://www.data.gov/community\">`__.

\\r

    228: Mensenrechten: `... <\"...\">`__ Groen! verzet zich tegen
    ongecontroleerde uitwisseling van persoonlijke gegevens
    `... <\"...\">`__

    \\r

    182: Privacy

    \\r

\\r

Dit zijn belangrijke aandachtspunten in het streven naar een Vrij
Internet, de link naar de informatiemaatschappij wordt echter niet
expliciet gelegd.

\\r

`Programma sp.a <\"http://www.s-p-a.be/media/uploads/programs/Volledig_kiesprogramma.pdf\">`__
==============================================================================================

\\r

    178: publieke dienstverlening `... <\"...\">`__ maximaal gebruik te
    maken van door de overheid gekende data, door digitale contactname
    mogelijk te maken\ `... <\"...\">`__

    \\r

\\r

Ook hier wordt het potentieel van Vrije Data en Open Standaarden niet
expliciet vermeld.

\\r

    82: We schenken bijzondere aandacht aan de bescherming van de
    privacy van de consumenten in het kader van nieuwe technologieën en
    sociale netwerken.

    \\r

\\r

Een concreet voorbeeld in verband met een Vrij Internet. Een gebruiker
kan maar over privacy op haar/zijn computer beschikken als zij/hij de
gebruikte software volledig kan onderzoeken en aanpassen, of dit kan
laten doen: kortom door Vrije Software te gebruiken.

\\r

`Progamma CD&V <\"http://verkiezingen.cdenv.be/sites/verkiezingen.cdenv.be/files/2010_Federaal%20Verkiezingsprogramma%20CD\">`__
================================================================================================================================

\\r

    Meer te investeren in het dichten van de digitale kloof. Een
    essentiële voorwaarde om uit te groeien tot een dynamische
    kennisregio.

    \\r

    (Belgacom:) `... <\"...\">`__ En laagdrempelige telecom-oplossingen
    te laten aanbieden die de digitale kloof meedichten.

    \\r

\\r

Hier worden de economische implicaties van de digitale kloof benadrukt.
Ook vanuit dit opzicht heeft Vrije Software een belangrijke rol te
spelen.

\\r

    De algemene invoering van een elektronisch en gedeeld medisch
    patiëntendossier is hierbij een belangrijk instrument.
    `... <\"...\">`__

    \\r

    Te werken aan een goed uitgebouwd E-health platform voor het beheer
    en de uitwisseling van informatie over de gezondheid van de patiënt,
    mits de privacy en het medisch geheim voldoende worden
    gerespecteerd. `... <\"...\">`__

    \\r

\\r

Bovenop de punten van Groen! wordt hier de aandacht gevestigd op de
privacy, nog een reden om hiervoor Vrije Software te gebruiken.

\\r

    Te komen tot de veralgemeende elektronische afhandeling van
    overheidsopdrachten. `... <\"...\">`__

    \\r

    E-government investeringen samen te laten gaan met initiatieven die
    de drempel naar het internetgebruik verlagen.

    \\r

\\r

Cfr. supra.

\\r

    Op het niveau van het Hof van Beroep moet een marktregulator
    gecreëerd worden voor problematieken inzake `... <\"...\">`__ ICT,
    mededinging. `... <\"...\">`__

    \\r

\\r

Niet alleen voor Vrije Software en Open Standaarden relevant.

\\r

    Informatisering justitie/politiediensten...

    \\r

\\r

Cfr. supra.

\\r

    Privacy...

    \\r

\\r

Wordt op verschillende plaatsen in het programma behandeld, en is
natuurlijk uiterst relevant voor vrijheid op het internet.

\\r

    Om auteursrechten, naburige rechten en leenvergoedingen beter en
    duidelijker te regelen. Gebruikers vinden hun weg niet meer in de
    veelheid aan regelgeving. `... <\"...\">`__

    \\r

    `... <\"...\">`__ mogelijkheden van strafvervolging met nieuwe
    technieken worden toegelaten, steeds met eerbiediging van de
    grondwettelijke rechten en vrijheden. Zo moet er regelgevend worden
    opgetreden voor `... <\"...\">`__ de dataretentie,
    informaticacriminaliteit, `... <\"...\">`__

    \\r

\\r

Goede afspraken in verband met deze zaken zijn zeer belangrijk voor
Digitale Vrijheid, maar het programma gaat niet in detail.

\\r

    `... <\"...\">`__ In samenspraak met de gemeenschappen wordt
    onderzocht of er een regeling kan komen voor non-profitorganisaties
    (verenigingen, musea, kunstencentra, ...) die vaak met
    overheidsmiddelen artistieke producten verspreiden en promoten.

    \\r

\\r

Wat de belastingbetaler financiert kan de overheid teruggeven als Vrije
Data. Dit kan de culturele impact van deze werken en de creativiteit
slechts ten goede komen.

\\r

`Programma Open VLD <\"http://pub.openvld.be/Bestanden/prog.pdf\">`__
=====================================================================

\\r

    Een vernieuwende en duurzame economie: `... <\"...\">`__ Ten slotte
    bestaat er ook veel creatief ondernemerschap in wat men
    “social-profit activiteiten” kan noemen. Voorbeelden: Wikipedia,
    open source software, `... <\"...\">`__

    \\r

\\r

In de marge vinden we hier ongeveer een vermelding van Vrije Software,
de term 'Open Source' ontwijkt namelijk de relevante ethische aspecten.
Het programma behandelt hier geen duidelijk standpunt of voorstel.

\\r

    Ook zal nog meer de kaart moeten getrokken worden van
    informatisering (E-Health). `... <\"...\">`__

    \\r

\\r

Cfr. supra.

\\r

    Liberalen hebben steeds mee aan de wieg gestaan in de strijd voor
    mensenrechten en persoonlijke vrijheid. `... <\"...\">`__

    \\r

\\r

De toepassing hiervan op de huidige informatiemaatschappij wordt echter
niet specifiek behandeld, ondanks het groeiende belang ervan. Privacy
wordt schijnbaar nergens behandeld in het programma.

\\r

    Het ‘Nieuw Strategisch Concept voor de NAVO’ moet antwoorden bieden
    op de uitdagingen van het komende decennium zoals: `... <\"...\">`__
    en cyber aanvallen. `... <\"...\">`__

    \\r

\\r

Wanneer het niet toegelaten is de werking van bijvoorbeeld een
besturingssysteem volledig te doorgronden kan er geen garantie zijn
tegen computerinbraak, via fouten in de software of via achterpoortjes.
Een onafhankelijke ICT-infrastructuur is slechts mogelijk met Vrije
Software.

\\r

`Programma N-VA <\"http://www.n-va.be/files/default/nva_images/documenten/verkiezingsprogramma%20N-VA%202010.pdf\">`__
======================================================================================================================

\\r

    5.4 Een ondernemingsvriendelijke overheid\ `
    ... <\"...\">`__ Vermindering van regeldruk voor ondernemers en een
    betere uitvoering gaat hand in hand met de invoering van
    standaardisatie van gegevens en processen met behulp van open
    standaarden. Op basis van een gegevensstandaard wordt een eenduidige
    vertaling van de wetgeving gemaakt in een gegevenswoordenboek dat
    ingebouwd wordt in de\\rsystemen van zowel de overheid als het
    bedrijfsleven.

    \\r

\\r

De enige expliciete vermelding van Open Standaarden in een Vlaams
verkiezingsprogramma!  Door transparantie in de structuur van de
gegevens verhindert men een monopoliepositie van één (of enkele)
bedrijven.

\\r

    4.6 Onze gezondheidszorg\ `
    ... <\"...\">`__ Verder pleiten we voor een verregaande
    digitalisering met respect voor de privacy `... <\"...\">`__

    \\r

\\r

Cfr. CD&V

\\r

    5.3 Durven afslanken\ `
    ... <\"...\">`__ in te zetten op een doorgedreven gebruik van ICT in
    de administratie `... <\"...\">`__

    \\r

\\r

Cfr. supra.

\\r

`Programma Lijst Dedecker <\"http://www.uwcenten.be/\">`__
==========================================================

\\r

In de sloganeske brochures wordt niets vermeld dat relevant is voor
Vrije Software, Vrije Data of een Vrij Internet.

\\r

`Programma Vlaams Belang <\"http://www.vlaamsbelang.org/2010_verkiezingsprogramma/\">`__
========================================================================================

\\r

Deze webpagina bestaat uit een Flash bestand dat niet kan bekeken worden
met `Gnash <\"http://www.gnu.org/software/gnash/\">`__, de vrije Flash
speler. Het programma is dan ook letterlijk niet aangepast aan Vrije
Software of Open Standaarden.

\\r

Conclusie
=========

\\r

In tegenstelling tot hun collega's bezuiden de taalgrens zijn de Vlaamse
politici (of hun partijideologen) dus blijkbaar nauwelijks op de hoogte
van de Digitale Vrijheden waarvoor we hun steun willen vragen. Toch
hebben vorig jaar bij de Europese en Vlaamse verkiezingen een aantal
onder hen het Vrije Software Pact ondertekend. Bij nader contact zal
moeten blijken in hoeverre zij geïnteresseerd zijn om zich achter deze
onderwerpen te scharen.

\\r

Vragen, opmerkingen en aanvullingen zijn welkom op het adres
`info@hetpact.be <\"mailto:info_@_hetpact.be\">`__

.. raw:: html

   </p>

