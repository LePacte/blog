Het pact bestaat volledig en 23 gecontacteerd kandidaten  voor Europa
#####################################################################
:date: 2009-06-05 05:01
:category: Européennes et régionales 2009
:slug: het-pact-bestaat-volledig-en-23-gecontacteerd-kandidaten-voor-europa
:status: published

Steven De Herdt beheert de actie in het Nederlands en schrijft

''Het pact bestaat volledig in het Nederlands en ik kan nu zeggen dat de
vlaamse campagne feitelijk begonnen is: ik heb 23 vlaamse kandidaten
voor Europa gecontacteerd. Benieuwd of ikantwoord zal krijgen ...''

Het pact is ook te downloaden in PDF-formaat op\ `het Vrije Software
pact 2009 in
pdf <\"http://candidatsbe.april.org/public/Vrije_Software_Pact_NL_be.pdf\">`__
of odt bewerkbare versie op `het Vrije Software pact 2009 in
odt <\"http://candidatsbe.april.org/public/Vrije_Software_Pact_NL_be.odt\">`__
met `de vrije en gratis Office-suite
OpenOffice.org <\"http://nl.openoffice.org\">`__.

(Pour ceux qui ne comprennent pas la langue de
`Vondel <\"http://fr.wikipedia.org/wiki/Joost_van_den_Vondel\">`__,
Steven De Herdt gère l'action en Néerlandais et écrit : *maintenant je
peux dire que la campagne flamande a vraiment commencé: j'ai contacté 23
candidats flamands pour l'Europe. Je suis curieux de savoir si je
recevrai des réponses...*)

.. raw:: html

   </p>

