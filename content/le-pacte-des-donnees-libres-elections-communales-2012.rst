Le Pacte des Données Libres - Élections communales 2012
#######################################################
:date: 2012-10-06 21:49
:slug: le-pacte-des-donnees-libres-elections-communales-2012
:status: published

Ce document est également téléchargeable en version pdf ici: `Pacte des
Données Libres 2012
(pdf) <\"public/Le_pacte_des_donnees_libres_2012.pdf\">`__.

\\r

Vous pouvez donc soit `signer le pacte de manière électronique et sûre
sur notre site <\"https://jesigne.lepacte.be/\">`__, soit remplir le
formulaire ici, imprimer cette page web, la signer et nous la renvoyer
par courrier, fax ou (scanné) par email, ou alors remplir
le\\rformulaire du pdf, imprimer le pdf, le signer et nous le renvoyer.

\\r

**LE\\rPACTE DES DONNÉES LIBRES 2012
Un bien commun à\\rprotéger et à développer**

\\r

**Je\\rsuis candidat(e) aux élections communales 2012**

\\r

**J'ai\\rconscience que**

\\r

\\rLes\\rdonnées libres sont fondées, comme les logiciels libres, sur
des\\rlicences qui soutiennent les droits des utilisateurs et, souvent,
sur les\\rbienfaits du travail collaboratif et de l'intelligence
collective.

\\r

\\rNos\\rsociétés démocratiques dépendent de la capacité [STRIKEOUT:du
gouvernement] de nos gouvernants et de l'administration publique à
offrir une transparence et un\\raccès libre aux informations non
nominatives qu'ils détiennent, et\\rpermettre ainsi à l'ensemble de la
société civile de participer\\ractivement. La création de ces données
publiques est financée par\\rle citoyen et l'utilité économique et
sociale de ces informations\\rest importante. La libéralisation des
données non nominatives est\\rdéjà une pratique courante dans plusieurs
pays. (voir par
exemple\\r\ `http://www.data.gov/community <\"http://www.data.gov/community\">`__)

\\r

\\rNos communes disposent d'un riche patrimoine historique et
principalement d'œuvres\\rdans le domaine public. Ce patrimoine, en plus
d'être une richesse\\rculturelle, est un vecteur potentiel pour
dynamiser l'économie du\\rsavoir et de la connaissance.

\\r

\\rDe\\rnombreux citoyens souhaitent participer au développement de
contenus\\rlibrement partageables avec le soutien des autorités, par
exemple\\rpour des données géographiques ou des contenus d'éducation.

\\r

**Je\\rm'engage donc à**

\\r

\\rSoutenir\\rles actions communales pour rendre les données non
nominatives\\rdétenues par la commune libres et accessibles à la
collectivité et\\rproduire à l'avenir davantage de données libres,
réutilisables,\\rdans des formats standards et ouverts, notamment par
des moyens\\rinstitutionnels.

\\r

\\rAssurer\\rque l'ensemble des citoyens puisse bénéficier de ces
données\\rnotamment en mettant à disposition gratuitement les accès
et\\rservices adéquats et ce à long terme.

\\r

\\rDynamiser\\rles échanges sur les analyses de ces données, permettre
au citoyen\\rde jouer un rôle actif.

\\r

\\rSoutenir\\rla numérisation et la diffusion publique des œuvres du
domaine\\rpublic.\\r

\\r

\\rEncourager\\rles initiatives citoyennes de création de contenus
librement\\rpartagés.

\\r

.. raw:: html

   <form name="\&quot;Form\&quot;">

\\r

.. raw:: html

   <div align="\&quot;JUSTIFY\&quot;">

\\r
\\rNom :\\r\\rPrénom : \\rParti :

\\r
Adresse :\\r

\\r
Code\\rpostal : \\rVille :

\\r
| Courriel :\\r\\rTel :
| Fait\\rle : \\rÀ :

\\r

.. raw:: html

   </div>

\\r

.. raw:: html

   </form>

\\r

\\rSignature :

\\r

\\rÀ renvoyer par\\rcourrier, fax ou (scanné) par email à :

\\r

\\r\ **Nicolas\\rPettiaux**

\\r

\\r\ **Avenue du\\rPérou 29 - 1000 Bruxelles**

\\r

\\r\ **Fax :\\r02 350 12 66 - email :
`jesigne@lepacte.be <\"mailto:jesigne@lepacte.be\">`__**

\\r

\\r\ **Contact :\\r\ `info@lepacte.be <\"mailto:info@lepacte.be\">`__ ou
gsm 0496 24\\r55 01**

| \\r

***Document\\rde l'initiative belge `LePacte.be <\"../\">`__\\rde
l'a.s.b.l. `À\\rl'Ère Libre <\"http://www.alerelibre.be/\">`__***

.. raw:: html

   </p>

