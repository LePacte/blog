Le pot du dimanche 21 juin 2009 à 18 h
######################################
:date: 2009-06-18 19:41
:category: Européennes et régionales 2009
:slug: le-pot-du-dimanche-21-juin-2009-a-18-h
:status: published

Après avoir bien travaillé ensemble par courriel, et en avoir discuté
par courriel, ceux qui veulent sont invités à se retrouver pour un pot,
histoire de mettre des visages et des voix\\rsur des noms et adresses
email.

\\r

La date qui convient au plus grand nombre : le **dimanche 21 juin 2009 à
18h**

\\r

Lieu : « **Etc...** », petit bistrot de quartier modernisé avec terasse
situé à Etterbeek – La Chasse, avenue Jules Malou, fréquenté par
quelques libristes avec wifi pour les amis.

\\r

La localisation sur la carte libre
`OpenStreetMap <\"http://www.osm.org/\">`__ est fournie
`http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17 <\"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\">`__
(au centre du quadrilatère rouge)

| \\rAccès :
| \\r

-  Tram 81 & 83 Bus 36, 59, 60, 95 (3/6 min)
-  Train par la gare Bruxelles/Luxembourg (20 min) ou bus 36 ou la gare
   d'Etterbeek (15 min) ou bus 95

\\rBières spéciales € 2.5 Jupiler € 1.4

Qui vient : allez voir sur
`doodle <\"http://doodle.com/r7kdnpihp7eagwzy\">`__ et éventuellement
vous inscrire (svp en indiquant votre nom réel)

\\r

Ce\\rn'est qu'une première rencontre. D'autres pourront suivre après le
15\\raoût si le coeur nous en dit. (ou avant sans Nicolas ce qui est
bien\\rsûr possible).

| \\r

.. raw:: html

   </p>

