22 signataires, l'ordre revient et le pacte est en ligne
########################################################
:date: 2009-06-03 19:36
:category: Européennes et régionales 2009
:slug: 22-signataires-lordre-revient-et-le-pacte-est-en-ligne
:status: published

Ce mercredi 3 juin 2009 à 4 jours des élections, nous avons enregistré
22 candidats signataires du pacte du logiciel libre en Belgique, pour
les élections régionales en Wallonie, à Bruxelles et pour l'Europe dans
le collège francophone. Le détails est toujours dans la `plate-forme
collaborative <\"http://candidatsbe.april.org/europarl2009/\">`__. Et
certains doivent encore être encodés très prochainement.

Et je peux vous annoncer que l'ordre revient : les candidats et les
signataires qui avaient été affectés à des mauvais collèges électoraux
sont progressivement affectés aux collèges réels. Cela rend
l'information plus juste et plus précise. Nous y tenons. Mais cela
prendra encore un peu de temps de terminer car nous avons déjà dans le
système plusieurs centaines de candidats (heureusement pas tous mal
affectés à leurs collèges respectifs mais qui doivent tous être vérifiés
!).

Enfin, les versions téléchargeables du pacte du logiciel libre sont
disponibles sur `le pacte du logiciel libre 2009 en
pdf <\"http://candidatsbe.april.org/public/Pacte_du_logiciel_libre_Fr_be.pdf\">`__
ou en version odt éditable sur `le pacte du logiciel libre 2009 en
odt <\"http://candidatsbe.april.org/public/Free-Software-Pact_FR_be.odt\">`__
éditable avec de nombreux outils parmi lesquels `la suite bureautique
libre et gratuite OpenOffice.org <\"http://fr.openoffice.org\">`__.

Merci aux signataires de porter et soutenir les valeurs du logiciel
libre,

Merci à tous les volontaires pour l'aide et

Continuons à sensibiliser les autres candidats qui ne connaîtraient pas
encore bien les multiples intérêts du logiciel libre (économiques,
sociaux, culturels, d'indépendance ...)

.. raw:: html

   </p>

