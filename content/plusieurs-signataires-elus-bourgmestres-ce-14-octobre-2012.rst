Plusieurs signataires élus bourgmestres ce 14 octobre 2012
##########################################################
:date: 2012-10-15 18:43
:category: Communales 2012
:slug: plusieurs-signataires-elus-bourgmestres-ce-14-octobre-2012
:status: published

Un premier examen des résultats montre que plusieurs signataires des
`pactes de libertés numériques <\"http://jesigne.lepacte.be\">`__ sont
des élus marquants. Ce billet reprendra quelques uns qui sont
bourgmestres :

\\r

-  `Olivier
   Deleuze <\"http://lepacte.be/communales2012/?action=editer_candidat&id=3211\">`__
   (Ecolo) à Watermael-Boisftfort,
-  `Elio di
   Rupo <\"http://lepacte.be/communales2012/?action=editer_candidat&id=2687\">`__
   (PS) à Mons,
-  `Jean-Michel
   Javaux <\"http://lepacte.be/communales2012/?action=editer_candidat&id=2532\">`__
   (Ecolo) à Amay.
-  `Paul
   Magnette <\"/communales2012/?action=editer_candidat&id=2899\">`__
   (PS) à Charleroi,
-  `Maxime
   Prévot <\"/communales2012/?action=editer_candidat&id=3004\">`__ (cdH)
   à Namur,\\r

\\r

Bravo à tous pour vos résultats. Nous nous réjouissons de vos élections
et de ce que cela augure dans les villes que vous allez diriger
concernant l'usage du logiciel libre.

\\r

Nous continuerons et rajouterons ici nos dépouillements des résultats et
comparaisons avec les signatures. Nous préciserons aussi les signataires
qui sont conseillers communaux et échevins.

\\r

Vous lecteurs pouvez nous aidez, le travail est important : il faut
chercher dans la `liste des
signataires <\"/communales2012/?action=liste_signataires&o_sens=DESC&o_date=1\">`__
et comparer aux listes des `élus
bruxellois <\"http://bru2012.irisnet.be/fr/index.html\">`__ et des `élus
wallons <\"http://elections2012.wallonie.be/results/fr/\">`__, et nous
envoyer un courriel à\ **`info @ lepacte.be <\"\">`__** pour que nous
puissions mettre le lien adéquat et adapter ce billet. Merci pour votre
aide.

.. raw:: html

   </p>

