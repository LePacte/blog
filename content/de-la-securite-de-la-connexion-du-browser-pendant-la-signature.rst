De la sécurité de la connexion du browser pendant la signature
##############################################################
:date: 2012-10-11 03:44
:category: Communales 2012
:slug: de-la-securite-de-la-connexion-du-browser-pendant-la-signature
:status: published

| À la deuxième étape de de signature, sur la page
`https://jesigne.lepacte.be/ <\"https://jesigne.lepacte.be/\">`__, les
utilisateurs voient le message :

**«Attention:** Vous allez être redirigé vers une\\rconnection
sécurisée, il est probable que votre browser émette une\\ralerte car le
certificat de ce site est signé par
`CACert.org <\"http://www.cacert.org\">`__,\\rune autorité de
certification collaborative qui n'est pas encore prise\\ren charge par
la totalité des browsers. Vous pouvez donc ignorer cette\\ralerte.»

\\r

| Les plus attentifs à la sécurité auront sans doute des difficultés à
ignorer l'alerte sans autre forme de procès.
| Voici donc quelques détails techniques derrière cette alerte.

\\r

L'initiative lepacte.be étant purement citoyenne et sans argent, ou
presque (tout dons est bienvenus merci ;-), nous utilisons
`cacert.org <\"http://cacert.org\">`__ comme autorité de certification .

\\r

\\rCela a deux inconvénients à l'heure actuelle:

\\r

-  CAcert n'est pour l'instant inclus que dans une fraction des browsers
-  Leur Root certificate repose toujours sur MD5

\\r

Cela a aussi des avantages

\\r

-  CAcert est un organisme sans but lucratif pris en main par un réseau
   communautaire, contrairement aux autres autorités de certification
   qui sont à vocation commerciale et vivent de la vente de certificats.
-  Ce réseau comportait en Janvier 2012 plus de 200,000 utilisateurs
   vérifiés et a produit quasi 800.000 certificats.
-  Obtenir un certificat est gratuit et l'identité du propriétaire du
   site utilisant le certificat a été vérifiée plusieurs fois par les
   Assureurs de la communauté CAcert. (notre responsable technique est
   Assureur CAcert soit-dit en passant)

\\r

En fait cela fait déjà longtemps que CAcert est entré dans un processus
long et difficile de redéfinition de leurs polices et après validation
de ces dernières ils devront générer de nouveaux certificats root
conformes à ces nouvelles procédures.

\\r

| Du coup, ils ont considéré que ce n'était pas la peine de s'esquinter
à renouveler le certificat root actuel tant que la procédure n'est pas
validée et mise en place.
| \\r
| \\rNotez que leur certificat intermédiaire (celui qui signe le nôtre
par exemple) utilise SHA256 et que les certificats tels que le nôtre
utilisent SHA1.
| \\r
| Malgré que le certificat root ait été généré avec MD5, une attaque
comme celle réalisée en 2008 est donc impossible ici. En effet si MD5
est cassé et qu'il est possible assez facilement de générer des
collisions, c-à-d deux textes qui donneront le même condensat, il est
toujours impossible à l'heure actuelle de monter une attaque du type
seconde pré-image, c-à-d trouver un autre certificat qui aurait le même
MD5 que le certificat root de CAcert.
| \\r
| Si vous voulez plus d'information, Google et Wikipedia sont vos amis.

.. raw:: html

   </p>

