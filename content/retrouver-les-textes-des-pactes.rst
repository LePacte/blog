Retrouver les textes des pactes
###############################
:date: 2013-10-07 07:25
:slug: retrouver-les-textes-des-pactes
:status: published

Les textes de pactes signés par les nombreux candidats à tous les
niveaux de pouvoir aux élections de 2009, 2010 et 2012 sont disponibles
sur le site
`https://jesigne.lepacte.be <\"https://jesigne.lepacte.be\">`__ (ainsi
que `http://jesigne.lepacte.be <\"http://jesigne.lepacte.be\">`__ en
connexion non sécurisée que nous déconseillons)

.. raw:: html

   </p>

