Pourquoi les libertés numériques sont importantes et les protéger aussi ?
#########################################################################
:date: 2012-10-12 08:50
:category: Communales 2012
:slug: pourquoi-les-libertes-numeriques-sont-importantes-et-les-proteger-aussi
:status: published

Il est très légitime de se demander pourquoi les libertés numériques
sont importantes et en quoi il faut les protéger.

\\r

Nous avons l'impression d'être libres avec nos ordinateurs, et que
de\\rplus en plus de logiciels libres sont utilisés. Par exemple,

\\r

-  `Mozilla Firefox <\"http://www.mozilla.org/fr/firefox/new/\">`__\\run
   navigateur  web, pour surfer en toute sécurité et\\rflexible grâce à
   une énorme collection de modules complémentaires optionnels.
-  `LibreOffice <\"http://fr.libreoffice.org/\">`__\\rune suite
   bureautique complète , comprenant des modules de :\\rtraitement de
   texte (Writer) ; tableur (Calc) ; de présentation\\r(Impress) ;
   réalisation de petite mise en page, affiches et dessin\\rvectoriel
   (Draw). Dans l'esprit particulier des logiciels libres,
   ces\\rlogiciels possèdent de nombreuses possibilité
   d'interopérabilités avec\\rd'autres logiciels et formats (libres ou
   privateurs). On entend par *privateur*, un format ou un logiciel qui
   prive l'utilisateur d'une des libertés garanties par `les licences de
   logiciels
   libres. <\"http://fr.wikipedia.org/wiki/Logiciel_libre\">`__
-  `Gimp <\"http://www.gimpfr.org/news.php\">`__ un puissant logiciel
   professionnel d'édition d'images matricielles, de retouche et
   composition photos,
-  `InkScape <\"http://www.inkscape-fr.org/\">`__, logiciel spécialisé
   dans le dessin vectoriels. Il est également idéal pour la réalisation
   graphique, pouvant combiner des éléments vectoriesl, des images,
   photos, textes et l'application de\\rfiltres non destructeurs. Ce
   logiciel est idéal pour la réalisation d'affiche, de carte de visite,
   logos, schémas, documents pédagogiques,
-  `Scribus <\"http://wiki.scribus.net/canvas/Page_principale\">`__ une
   application de PAO (publication assistée par ordinateur)
   professionnelle crédible auprès des imprimeurs les plus exigeants. Il
   gère donc les profils de couleur, l'incontournable\\rquadrichromie et
   l'export en PDF version 1.3, 1.4, 1.5 ou même X-3.\\r

\\r

et beaucoup d'autres pour la manipulation ou création
de\\rsons,\\rmusiques, l'écriture musicale, le montage vidéos, la
création de films d'animations, la création de modèles et animations en
2D et 3D, composition des plans 2D et 3D, des jeux, le pilotage de
machines et expériences, l'analyse de résultats d'expériences, la
modélisation et l'expérimentation ...

\\r

Toujours au chapitre des bonnes nouvelles, plus de la moitié des
administrations\\rcommunales wallones et plusieurs administrations
bruxelloises par\\rexemple, utilisent la suite de logiciels connues sous
le nom `CommunesPlone  <\"http://www.communesplone.org/\">`__ devenu
l'intercommunale `IMIO <\"http://www.imio.be\">`__ il y a peu,
développé\\rpar ces administrations avec de nombreux prestataires
petites ou\\rgrandes entreprises de service, et maintenus par elles.

\\r

Mais il y a aussi de nombreux soucis et difficultés.
====================================================

\\r

Dans les écoles, du primaire à l'université, trop souvent, les\\renfants
et étudiants apprennent la manipulation d'outils informatiques\\rdont
ils ne peuvent pas comprendre, car il est sciemment dissimulé
et\\rinterdit d'accès, le fonctionnement. Un peu comme si on étudiait
la\\rmédecine uniquement par auscultation que l'anatomie et les
dissections\\rétaient interdites. De plus, ces formations rendent les
apprenants\\rdépendants d'outils qui, si ils sont mis à disposition
souvent\\rgracieusement, entraînent la création d'une dépendance comme
dans toute\\rformation, mais oblige par la suite les utilisateurs à
acheter les\\rproduits une fois arrivés dans la vie professionnelle.
Alors qu'ils\\rauraient pu autrement poursuivre avec les outils appris,
dont il\\rpeuvent, eux-même ou par délégation, étendre les
fonctionnalités.

\\r

\\rDes administrations belges, parfois très grandes, achètent et
installent encore aujourd'hui des «\\rlogiciels bureautiques privateurs
» (habituels pour beaucoup) sans appel\\rd'offre général au\\rmarché
parfois pour des sommes impressionnantes, alors que des\\ralternatives
libres, moins chères existent et ont été choisies par de\\rtrès grandes
administrations et institutions notamment étrangères dans\\rune volonté
de contrôle total de leurs systèmes d'information.

\\r

Les\\rfabricants d'ordinateurs et surtout les magasins continuent à
imposer\\rdes logiciels privateurs et coûteux lors de l'achat d'un
ordinateur. L'acheteur, s'il ne souhaite pas utiliser ces
logiciels\\rdevra souvent assumer le prix de démarches juridiques
laborieuse pour obtenir gain de cause et se faire rembourser. Quoi que
l'on en dise\\rparfois, il est difficile de comparer cette situation à
d'autre biens de consommation. On pourrait imaginer une solution où
plusieurs systèmes\\rd'exploitation complètement fonctionnels sont
installés par défaut sur\\rle disque permanent de l'ordinateur convoité,
et que le consommateur se\\rvoit informé du prix de la machine nue et en
option, des prix des\\rdifférents systèmes qui pourraient aussi
coexister si il en fait le\\rchoix informé ... et qu'il a payé. Il est
aujourd'hui parfaitement\\rpossible, d'emporter un ordinateur sans
logiciels installés et simple et\\rrapide d'installer soit-même, ou à
l'aide d'une tierce personne, tout\\rles logiciels nécessaires tel qu'un
système d'exploitation (Ubuntu\\rGNU/Linux pour être libre par exemple),
suite bureautique, navigateur web, etc. La situation d'une vente lié et
forcé ne se justifie donc pas\\ret s'apparente volontiers à du racket.
Les logiciels privateurs concerné sont souvent les mêmes et ne sont
ainsi pas uniquement imposé de manière économique et fonctionnelle mais
également de manière psychologique et pédagogique. Ainsi, cela donne la
fausse impression que seul ces logiciels existent et sont utilisables.
Ces\\rventes forcées, pour lesquels le système d'exploitation aurait pu
être\\racheté séparément au prix d'environ 100 €, et qui représente donc
une\\rlarge partie du coût\\rd'achat de l'ordinateur (on trouve de très
très bons portables pour 300 à\\r800 €) sont en contradiction avec la
Directive
`2005/29/CE <\"http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32005L0029:FR:NOT\">`__\\rdu
11 mai 2005 relative aux `pratiques commerciales
déloyales <\"http://europa.eu/legislation_summaries/consumers/consumer_information/l32011_fr.htm\">`__
des\\rentreprises vis-à-vis des consommateurs dans le marché intérieur
et\\rmodifiant les directives 84/450/CEE, 97/7/CE, 98/27/CE et
2002/65/CE et\\rle règlement (CE) n° 2006/2004 (directive sur les
pratiques commerciales\\rdéloyales).

\\r

Avec les usages actuels de la téléphonie portable et
d'internet\\ractuel, nous sommes en permanence, souvent à notre insu,
surveillés,\\rcensurés. Certains veulent limiter les usages que nous
faisons\\rd'internet, de notre droit de copier, même dans le cadre
familial de la\\rsphère privée explicitement autorisé par la loi. C'est
ce que nous avons\\rconnu récemment avec le fameux dossier ACTA, mais
une nouvelle menace\\rapproche, très inspirée d'ACTA et qui porte le nom
de CETA.

\\r

Enfin,\\rla créativité est elle-même menacée par l'inflation aberrante
de\\r«brevets sur des logiciels et méthodes» qui rendent le
développement\\rinformatique, de logiciels libres autant que de
logiciels privateurs,\\rune parcours risqué semblable à la traversée
d'un champ de mines, car le\\rrisque juridique est toujours présent :
être poursuivi pour contrefaçon\\rde brevet, et même si c'est faux,
devoir dépenser de telles sommes en\\rfrais d'avocats que ceci assèche
même les mieux préparés.

.. raw:: html

   </p>

