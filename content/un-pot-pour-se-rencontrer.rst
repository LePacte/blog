Un pot pour se rencontrer
#########################
:date: 2009-06-13 14:35
:category: Européennes et régionales 2009
:slug: un-pot-pour-se-rencontrer
:status: published

| Après avoir bien travaillé ensemble par courriel, Nicolas vous propose
de\\rse retrouver pour un pot, histoire de mettre des visages et des
voix\\rsur des noms et adresses email.
| \\r
| Lieu : Bruxelles, un café avec une terrasse, près d'une gare pour un
accès simple.
| Pour fixer la date, qui a l'air à cette heure d'être le **dimanche 21
juin 2009 à 18h** et savoir qui vient, allez vous inscrire (svp en
indiquant votre nom réel) sur
`doodle <\"http://doodle.com/r7kdnpihp7eagwzy\">`__.
| Ce n'est qu'une première rencontre. D'autres pourront suivre après le
15 août si le coeur nous en dit. (ou avant sans Nicolas ce qui est bien
sûr possible).
| Vincent «Max» nous propose 3 endroits, selon l'ordre de ses habitudes.
La première plaît le plus à Phil Teuwen et Nicolas Pettiaux. Dites nous
sur la liste ou en commentaire sur doodle quand vous vous inscrivez, ce
que vous en pensez.

« `Etc... <\"http://fr.wikipedia.org/wiki/Liste_de_bars_et_caf%C3%A9s_bruxellois\">`__ »
========================================================================================

| \\rPetit bistrot de quartier modernisé avec terasse situé à Etterbeek
– La Chasse, `avenue Jules
Malou <\"http://maps.google.be/maps?f=q&source=s_q&hl=fr&geocode=&q=Etterbeek,+malou&sll=50.838251,4.386978&sspn=0.051385,0.140247&ie=UTF8&ll=50.831827,4.387708&spn=0.012848,0.035062&z=15&iwloc=A\">`__
| Fréquenté par quelques libristes, wifi pour les amis. La localisation
sur la carte libre `OpenStreetMap <\"http://www.osm.org\">`__ est
fournie
`|\\"\\"|\ http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17 <\"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\">`__
(au centre du quadrilatère rouge)
| Accès :

-  Tram 81 & 83 Bus 36, 59, 60, 95 (3/6 min)
-  Train par la gare Bruxelles/Luxembourg (20 min) ou bus 36 ou la gare
   d'Etterbeek (15 min) ou bus 95

| \\rBières spéciales € 2.5 Jupiler € 1.4

`« Les Postiers » <\"http://www.lespostiers.com/fr/acc/fr.htm\">`__
===================================================================

| \\rTaverne/Brasserie bruxelloise classique, avec une terrasse un peu
bruyante et venteuse et une grande salle.
| Hyper-centre de Bruxelles, rue Fossé aux loups (Monnaie)
|  (le hot spot ne fonctionne plus)
| Assez cher. Il y a à manger.
| Accès : 5 à 7 minutes de la gare centrale à pied, nombreux trams et
bus

« Fabian O'Farell's Pub »
=========================

| \\rIrish pub, tout est dit.
| Place Luxembourg, rue d'Arlon
| Accès : à 2 minutes à pied de la gare Luxembourg, nombreux bus.
| Assez cher, mais c'est du Guiness !

.. raw:: html

   </p>

.. |\\"\\"| image:: \"http://www.openstreetmap.org/index.html?mlat=50.831658&mlon=4.385697&zoom=17\"
